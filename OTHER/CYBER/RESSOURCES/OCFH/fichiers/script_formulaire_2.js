function validateForm() {
    let x = document.forms["myForm"]["fPassword"].value; // On récupère la valeur de "fPassword"

    if (x.match("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -/:-@\[-`{-~]).{8,16}$") == null)

        // la ligne du dessus verifie ci cette valeur correspond bien au motif désiré (voir plus bas)
    {
        alert("Le mot de passe doit contenir une minuscule, une majuscule, un chiffre, un caractère spécial et " +
            "comporter entre 8 et 16 caractères.");
        document.forms["myForm"]["fPassword"].value = ""; // On vide le champ fPassword
        return false;
    }
}

/* EXPLICATION DU MATCH

^ : début de la chaine

(?=.*[a-z]) : La chaine commence par un caractère, n'importe lequel (grâce à .) et en n'importe quel nombre de fois (*)
Puis elle a un caractère qui est une lettre minuscule

(?=.*[A-Z])  : La chaine contient une minuscule

(?=.*[0-9]) : elle contient un chiffre

(?=.*[ -/:-@\[-{-~])` : et un caractère spécial

.{8,16} et elle est de longueur 8 à 16

$ : fin de la chaîne


 */

