import string

ALPHABET = string.ascii_uppercase
print(ALPHABET)


def produit_table(motif: str) -> dict:
    resultat = {}
    p = len(motif)
    for lettre in ALPHABET:
        resultat[lettre] = p
    for i in range(p - 1):
        resultat[motif[i]] = p - 1 - i
    return resultat


def bmh(texte: str, motif: str) -> int:
    decale = produit_table(motif)
    n = len(texte)
    p = len(motif)
    i = p - 1
    while i < n:
        j = 0
        while j < p and texte[i - j] == motif[p - 1 - j]:
            j += 1
        if j == p:
            return i - p + 1
        else:
            print(j)
            i += decale[texte[i - j]] - j
    return -1


print(bmh("CANIQUESTITUTIONNELLEMENT", "CONIQUE"))
print(produit_table("RIO"))
