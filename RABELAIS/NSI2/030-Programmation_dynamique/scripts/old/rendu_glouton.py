liste_valeurs = [500, 200, 100, 50, 20, 10, 5, 2, 1]  # la liste doit être triée dans l'ordre décroissant


def rendu_monnaie(montant: int) -> list[int]:
    liste_rendu = []
    for valeur in liste_valeurs:  # on parcourt la liste en commençant donc par les grosses valeurs
        while montant >= valeur:  # si on peut rendre la monnaie
            montant -= valeur  # alors, on le fait
            liste_rendu.append(valeur)
        # en fin de boucle montant est trop petit pour la valeur courante, on passe
        # donc à la prochaine valeur de la liste, plus petite
    return liste_rendu
