solutions = [1, 1]  # on sait que les 2 premiers termes sont 1


def fibo_dyn(n: int) -> int:
    if n >= len(solutions):  # si solutions[n] n'existe pas
        r = fibo_dyn(n - 1) + fibo_dyn(n - 2)  # on calcule récursivement
        solutions.append(r)  # puis on stocke la solution
        return r  # et on la renvoie
    else:
        return solutions[n]  # sinon on renvoie la solution déjà calculée


print(fibo_dyn(500))
