def edition(mot1: str, mot2: str) -> int:
    if not mot1:
        return len(mot2)
    elif not mot2:
        return len(mot1)
    elif mot1[0] == mot2[0]:
        return edition(mot1[1:], mot2[1:])
    else:
        return 1 + min(edition(mot1[1:], mot2), edition(mot1, mot2[1:]))


def edition2(mot1: str, mot2: str) -> tuple[int, str, str]:
    if not mot1:
        return len(mot2), "-"*len(mot2), mot2
    elif not mot2:
        return len(mot1), mot1, "-"*len(mot1)
    elif mot1[0] == mot2[0]:
        d, m, n = edition2(mot1[1:], mot2[1:])
        return d, mot1[0]+m, mot2[0]+n
    else:
        d1, m1, n1 = edition2(mot1, mot2[1:])
        d2, m2, n2 = edition2(mot1[1:], mot2)
        if d1 <= d2:
            return 1+d1, "-"+m1, mot2[0]+n1
        else:
            return 1+d2, mot1[0]+m2, "-"+n2


print(edition2('BEC', 'BAC'))

print(edition2('INFORMATIQUE', 'NUMERIQUE'))
