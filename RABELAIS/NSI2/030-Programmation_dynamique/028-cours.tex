\documentclass[10pt,firamath,cours]{nsi}
\begin{document}
\setcounter{chapter}{25}
\chapter{Programmation dynamique}

\section{Un exemple bien connu}

Nous commençons à bien connaître la suite de Fibonacci

$$F_n=\begin{cases}1 & \mbox{si } n=0\mbox{ ou }n=1\\ F_{n-1}+F_{n-2} &\mbox{sinon}\end{cases}$$

Si on la programme \emph{naïvement} de manière récursive, voici ce qu'on obtient :

\begin{pyc}
    \begin{minted}{python}
def fibo(n: int) -> int:
    if n < 2:
        return 1
    else:
        return fibo(n - 1) + fibo(n - 2)
    \end{minted}
\end{pyc}

Or, voici l'arbre des appels récursifs lors de l'exécution de \mintinline{python}{fibo(4)}  :

\begin{center}
    \includegraphics[width=8cm]{img/fibo_4.pdf}
\end{center}


Et pour \mintinline{python}{fibo(5)}  on obtient :

\begin{center}
    \includegraphics[width=12cm]{img/fibo_5.pdf}
\end{center}

Lors de cet appel, \mintinline{python}{fibo(3)}  est appelée 2 fois, \mintinline{python}{fibo(2)}  3 fois, \mintinline{python}{fibo(1)}  5 fois et \mintinline{python}{fibo(0)}  3 fois !

Il n'est donc pas étonnant que \mintinline{python}{fibo(40)}  prenne quelques secondes. Quant à \mintinline{python}{fibo(100)} , ce n'est même pas la peine d'y penser !

Notre stratégie, de type « diviser pour régner », décompose le problème du calcul de \mintinline{python}{fibo(n)}  en 2 sous-problèmes plus simples, mais \emph{ces problèmes ne sont pas indépendants} , comme l'illustre le graphe suivant, obtenu à partir du schéma précédent.

\begin{center}
    \includegraphics[width=4cm]{img/fibo_5_graphe.pdf}
\end{center}

\section{La mémoïsation}

\begin{definition}[ : mémoïsation]
    C'est le fait de garder en mémoire les solutions des sous-problèmes pour pouvoir les réutiliser sans avoir à les déterminer de nouveau.
\end{definition}

Cela veut dire qu'avant de renvoyer un résultat, la fonction le stocke (dans une liste ou un dictionnaire). 

Ce qui est fort agréable, c'est que les listes et les dictionnaires sont de \emph{type mutable}, ce qui nous permet d'écrire
ceci :

\begin{pyc}
    \begin{minted}{python}
solutions = [1, 1]  # on sait que les 2 premiers termes sont 1

def fibo_dyn(n: int) -> int:
    if n >= len(solutions):  # si solutions[n] n'existe pas
        r = fibo_dyn(n - 1) + fibo_dyn(n - 2)  # on calcule récursivement
        solutions.append(r)  # puis on stocke la solution
        return r  # et on la renvoie
    else:
        return solutions[n]  # sinon on renvoie la solution déjà calculée

print(fibo_dyn(500))
    \end{minted}
\end{pyc}

Ce qui est un peu délicat à comprendre, c'est que la ligne \mintinline{python}{solutions.append(r)}  est telle que l'entier \mintinline{python}{r}  est placé « au bon endroit » dans la liste \mintinline{python}{solutions}.

Mais en procédant ainsi on peut calculer rapidement que $F_{40}=165580141$, et même que


$F_{500}$ vaut 225591516161936330872512695036072072046011324913758190588638866418474627738\\686883405015987052796968498626.\\

Le fait d'utiliser la mémoïsation rend les choses bien plus rapides !


\begin{exercice}[]
    Reprendre ce qui a été fait précédemment mais en mémoïsant les solutions à l'aide d'un dictionnaire (c'est plus simple qu'avec une liste et plus général).

    Comparer ensuite l'efficacité de la fonction avec et sans mémoïsation.
   
\end{exercice}

    

\begin{remarque}[ importante]
    L'exemple précédent est \emph{pédagogique} car il montre bien l'intérêt de la mémoïsation. Cependant, il n'y a pas besoin d'utiliser cette technique pour avoir une fonction efficace : il suffit de programmer de manière \emph{itérative} et non
    récursive :
    \begin{minted}{python}
def fibo_iteratif(n: int) -> int:
    a, b = 1, 1  # on initialise a = F0 et b = F1
    for _ in range(n - 1):
        a, b = b, a + b  # a devient b et b devient a + b
    return b
    \end{minted}  
\end{remarque} 


Nous allons maintenant examiner d'autres exemples pour lesquels

\begin{itemize}
    \item	on ne voit pas de version itérative évidente (ce qui ne veut pas dire qu'il n'y en a pas) !
    \item	les sous-problèmes ne sont pas indépendants : sans programmation dynamique, on calculerait plusieurs fois les sous
    solutions.
\end{itemize}

\section{Rendu de monnaie}

Nous avons déjà rencontré cette situation l'année dernière, on y appliquait un algorithme \emph{glouton} : pour rendre un
montant donné, on commence par rendre les billets/pièces qui ont le plus de valeur et ainsi de suite, ce qui donne lieu au programme suivant :

\begin{pyc}
    \begin{minted}{python}
liste_valeurs = [500, 200, 100, 50, 20, 10, 5, 2, 1]  # la liste doit être triée dans l'ordre décroissant


def rendu_monnaie(montant: int) -> list[int]:
    liste_rendu = []
    for valeur in liste_valeurs:  # on parcourt la liste en commençant donc par les grosses valeurs
        while montant >= valeur:  # si on peut rendre la monnaie
            montant -= valeur  # alors, on le fait
            liste_rendu.append(valeur)
        # en fin de boucle montant est trop petit pour la valeur courante, on passe
        # donc à la prochaine valeur de la liste, plus petite
    return liste_rendu
    \end{minted}
\end{pyc}

Avec le système euro, on obtient par exemple
\begin{minted}{pycon}
>>> rendu_monnaie(47)

[20, 20, 5, 2]
\end{minted}
Ce qui nous donne un nombre minimal de pièces/billets rendus.

Avec le système euro, l'algorithme glouton donne toujours la solution optimale, mais ce n'est pas toujours le cas : par exemple, en utilisant l'ancien système monétaire anglais :

\begin{minted}{pycon}
>>> liste_valeurs = [30, 24, 12, 6, 3, 1]  # ancien système anglais
>>> rendu_monnaie(48)

[30, 12, 6]
\end{minted}

Or la solution optimale est \mintinline{python}{[24, 24]}.\\ 

Notre algorithme glouton ne convient donc pas dans toutes les situations. Or, nous voulons, pour un système monétaire
donné, trouver \emph{le plus petit nombre de pièces/billets nécessaires} pour rendre une somme quelconque.\\

On va utiliser une méthode de type « diviser pour régner ». Soit 

\begin{itemize}
    \item $n$ la somme à rendre, strictement positive ; 
    \item $\mathscr{P}=\{P_0;...;P_k\}$ le système monétaire ;
    \item $R(n)$ le nombre minimum de pièces pour rendre $n$ selon $\mathscr{P}$.
\end{itemize}

Alors :
\begin{itemize}
    \item	si $n\in\mathscr{P}$ cela veut dire qu'une pièce suffit et alors $R(n)=1$.

    \item	sinon, appelons $\mathscr{P}_n=\{x \in\mathscr{P}, x < n\}$ l'ensemble des pièces qui valent moins que $n$. Il suffit de 
    \begin{itemize}
        \item	regarder ce que vaut $R(n-x)$ pour tout $x\in\mathscr{P}_n$ ;
        \item	prendre le minimum $m$ de toutes ces valeurs;
    \end{itemize}
    et ainsi $R(n)= 1 + m$.
\end{itemize}

En définitive
      
      $$R_n=\begin{cases}1 & \mbox{si } n\in\mathscr{P}\\ 
       1+ min(\{R(n-x), x\in\mathscr{P}, x<n\}) &\mbox{sinon}\end{cases}$$
      
On peut illustrer cette méthode pour rendre 7 livres dans l'ancien système monétaire anglais :

\begin{center}
    \includegraphics[width=10cm]{img/graphe_rendu.pdf}
\end{center}

On se rend compte que 2 pieces suffisent pour rendre la monnaie, mais on voit aussi que $R(4)$ est calculé 2 fois, ainsi 
que $R(3)$, \emph{et c\ae tera} : les sous-problèmes sont loins d'être indépendants, la programmation dynamique va nous aider !

\begin{exercice}[]
    Programmation dynamique du rendu de monnaie optimal avec mémoïsation.
\end{exercice}
\begin{center}
    \includegraphics[width=10cm]{img/memoisation.png}\\
    \emph{Cette illustration a été générée par DALL$\cdot$E 3 et n'a aucun caractère religieux.}
\end{center}

\section{Alignement de séquences}


On dispose de deux chaînes de caractères : \mintinline{python}{"INFORMATIQUE"} et \mintinline{python}{"NUMERIQUE"}.\\
On aimerait mettre ces deux chaînes de caractères en correspondance de la manière suivante :

\begin{itemize}
    \item on place les 2 chaînes l'une en dessous de l'autre ;
    \item si les derniers caractères des deux chaînes coïncident, alors on passe aux caractères suivants ;
    \item sinon, on va ajouter un trou dans une des deux chaînes (mais laquelle ?), symbolisé par un \mintinline{python}{"_"} et on passe aux caractères suivants.
\end{itemize}


Voici ce que cela donne :
\begin{center}
    \includegraphics[height=1.05cm]{img/corresp_info_num}
\end{center}
Dans cette situation, on a besoin de 9 tirets, pas moins.\\

L'objectif est d'aligner le \emph{maximum} de lettres (donc de mettre le moins de \mintinline{python}{"_"} possible).
Ce n'est pas un problème simple, surtout quand les chaînes sont longues :
\begin{center}
    \includegraphics[height=1.05cm]{img/corr_adn}
\end{center}

Ici pas moins de 26 tirets sont nécessaires.\\

Cette technique est utilisée en biologie pour mettre (entre autres) en évidence des parties 
communes à deux séquences d'ADN.\\

Encore une fois, on décompose un problème en sous-problèmes non-indépendants, comme le montre l'exemple suivant, qui 
nécessite 4 tirets :

\begin{center}
    \includegraphics[height=1.05cm]{img/006}
\end{center}



Et dont le graphe de résolution est le suivant :

\begin{itemize}
    \item une flèche rouge signifie que les 2 lettres courantes coïncident ;
    \item une flèche bleue signifie qu'on choisit d'ajouter un tiret en bas ;
    \item une verte signifie qu'on a ajouté un tiret en haut ;
    \item on n'écrit dans les nœuds que les lettres non traitées.
\end{itemize}



\begin{center}
    \includegraphics[width=10cm]{img/align-graphe.pdf}
\end{center}

\begin{exercice}[]
    Quel est le chemin qui correspond à la solution ? 

    %Bas, gauche, gauche, droite, droite, bas.
\end{exercice}


Le graphe de résolution n'est pas un arbre : les sous-problèmes ne sont pas indépendants, on a tout intérêt à utiliser 
la programmation dynamique.\\

Notons aussi qu'on peut résoudre le problème de 2 manières :

\begin{itemize}
    \item	on peut aligner les 2 chaînes « à gauche » et procéder sans le sens de la lecture ;
    \item	on peut aussi les aligner « à droite » et procéder de droite à gauche.
\end{itemize}

Enfin, il n'y a pas toujours \emph{unicité} de la solution, comme le prouve l'exemple ci-dessous :

\begin{exemple}[]
Pour aligner \mintinline{python}{"BEC"} et \mintinline{python}{"BAC"} on peut 
\begin{itemize}
    \item	considérer \mintinline{python}{"B-EC"} et \mintinline{python}{"BA-C"} ;
    \item   ou bien \mintinline{python}{"BE-C"} et \mintinline{python}{"B-AC"}.
\end{itemize}   
\end{exemple}

Il y a au moins deux méthodes pour programmer cet algorithme :

\begin{itemize}
    \item	récursivement, comme on l'a fait pour le rendu de monnaie ;
    \item	itérativement, en remplissant au fur et à mesure une liste à deux dimensions (on ne le fera pas).
\end{itemize}

Le « nombre de tirets » minimum qu'il faut utiliser pour mettre en correspondance les deux motifs s'appelle la \emph{distance d'édition} qui sépare ces deux motifs.\\
Par exemple, la distance d'édition entre \mintinline{python}{"BASSE"} et \mintinline{python}{"BUSE"} est 3 (on l'a vu) et ce pour une raison simple : pour passer de  \mintinline{python}{"BASSE"} à \mintinline{python}{"BUSE"} 
\begin{itemize}
    \item	on part de \mintinline{python}{"BASSE"} ;
    \item	on enlève le \mintinline{python}{"A"} ;
    \item   on le remplace par un \mintinline{python}{"U"} ;
    \item   on retire un \mintinline{python}{"S"}
    \item   on a fait 3 opérations d'édition.  
\end{itemize}
\begin{exercice}[]
\begin{enumerate}
    \item	Écrire la fonction \mintinline{python}{distance} qui
    \begin{itemize}
        \item	en entrée prend deux \mintinline{python}{str} \mintinline{python}{mot1} et \mintinline{python}{mot2} ;   
        \item	renvoie la distance d'édition entre ces deux chaînes.
    \end{itemize}    
    
    \item Tester la fonction sur 
    \begin{itemize}
        \item	\mintinline{python}{"BASSE"} et \mintinline{python}{"BUSE"} ;
        \item	\mintinline{python}{"NUMERIQUE"} et \mintinline{python}{"INFORMATIQUE"} ;
        \item \mintinline{python}{"TTCACCAGAAAAGAACACGGTAGTTACGAGTCCAATATTGTTAAACCG"} et 
        \mintinline{python}{"TTCACGAAAAAGTAACGGGCCGATCTCCAATAAGTGCGACCGAG"} ; 
    \end{itemize}
    \item Améliorer la vitesse d'exécution de la fonction en mémoïsant.
\end{enumerate}
\end{exercice}

\begin{exercice}[$\bigstar$]
    Reprendre la fonction précédente et la transformer pour qu'elle ne renvoie pas seulement la distance d'édition mais également l'alignement des deux motifs en utilisant des tirets, comme dans les exemples (on pourra faire en sorte qu'elle renvoie un \mintinline{python}{tuple} comportant la distance, la première chaîne avec des tirets et la deuxième avec des tirets=).
\end{exercice}

\end{document}