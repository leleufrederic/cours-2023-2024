class Cellule:
    def __init__(self, valeur, suivant=None):
        self.valeur = valeur
        self.suivant = suivant

    def __repr__(self) -> str:
        courant = self
        resultat = []
        while courant:
            resultat.append(str(courant.valeur))
            courant = courant.suivant
        return " -> ".join(resultat)

    def __len__(self) -> int:
        resultat = 0
        courant = self
        while courant is not None:
            resultat = resultat + 1
            courant = courant.suivant
        return resultat

    def getcell(self, indice: int):
        courant = self
        while indice and courant is not None:
            courant = courant.suivant
            indice -= 1
        if courant is None:
            raise IndexError("l'indice est trop grand.")
        else:
            return courant

    def __getitem__(self, indice: int):
        return self.getcell(indice).valeur


class Tete:
    def __init__(self):
        self.debut = None

    def __len__(self) -> int:
        return 0 if self.debut is None else len(self.debut)

    def __repr__(self) -> str:
        return "<vide>" if self.debut is None else str(self.debut)

    def __getitem__(self, indice):

        if not self.debut:
            raise IndexError("Liste vide.")
        else:
            try:
                return self.debut[indice]
            except IndexError:
                raise IndexError("l'indice est trop grand.")

    def insert(self, indice, valeur):
        if indice == 0:
            nouveau = Cellule(valeur, self.debut)
            self.debut = nouveau
        else:
            courant = self.debut.getcell(indice - 1)
            nouveau = Cellule(valeur, courant.suivant)
            courant.suivant = nouveau

    def remove(self, indice):
        if indice == 0:
            if self.debut is None:
                raise IndexError("l'indice est trop grand.")
            else:
                self.debut = self.debut.suivant
        else:
            courant = self.debut.getcell(indice - 1)
            if courant.suivant is None:
                raise IndexError("l'indice est trop grand.")
            else:
                courant.suivant = courant.suivant.suivant


lst = Tete()
lst.insert(0, 10)
lst.insert(1, 11)
lst.insert(2, 12)
lst.insert(8, 9)
print(lst)
lst.remove(3)
print(lst)
