\documentclass[12pt,firamath,cours]{nsi}
\begin{document}
\setcounter{chapter}{22}

\chapter{Complexité}

\begin{encadrecolore}{À retenir}{UGLiGreen}
   \begin{itemize}
      \item	Étudier la complexité temporelle d'un algorithme, c'est déterminer comment le nombre d'opérations élémentaires effectuées par celui-ci augmente en fonction de la taille du problème qu'il résout ;
      \item	On étudie surtout la complexité \emph{pire cas}, celle qui produit le maximum d'opels pour une taille de problème donnée ;
      \item Les résultats ne sont pas toujours formulés de manière exacte mais sont exprimés à l'aide de fonctions de référence : constant, logarithmique, racinaire, linéaire, carré, exponentiel\ldots.
   \end{itemize}
\end{encadrecolore}

\section{La fonction « puissance de 2 »}
\dleft{4.4cm}{
   \begin{center}
      \begin{tikzpicture}[scale=14/21]
         \reperevl{-2}{-1}{5}{12}
         \clip(-2,-1) rectangle (5,12);
         \draw[domain=-2:5, smooth, samples = 500,variable=\x, thick, UGLiRed] plot ({\x}, {2^(\x))});
         
         \pointc{3}{8}{3}{$2^3=8$}{}
      \end{tikzpicture}
      
      
   \end{center}
}
{Depuis le collège on sait calculer des puissances entières de 2 :
   \begin{itemize}
      \item	$2^4=2\times 2\times 2\times 2=16$ ;
      \item	$2^{-3}=\dfrac{1}{2^3}=\dfrac{1}{8}$ ;
   \end{itemize}
   
   On admet que cela se généralise et qu'il est possible de définir ce que vaut $2^x$ pour tout $x\in\R$, c'est-à-dire même lorsque $x$ n'est pas entier.\\
   
   La fonction $x\mapsto 2^x$ est strictement croissante sur $\R$ et croît extrêmement rapidement.\\
   
   Sa courbe représentative est dessiné ci-contre.
   
}

\section{La fonction logarithme binaire}


On sait déjà que  :
\begin{itemize}
   \item 4 est une puissance de 2 car c'est $2^2$.
   \item $0,125=\dfrac{1}{8}=2^{-3}$.
   \item $\sqrt{2}=2^{\frac{1}{2}}$ puisque $\left(\sqrt{2}\right)^2=2^1$.
\end{itemize}
Cela se généralise...

\begin{definition}[ : fonction logarithme binaire]
   On admet que tout réel strictement positif $x$ est une puissance de 2 (pas nécessairement entière) : 
   
   Pour tout $x\in\oii{0}$ il existe un unique réel $y$ tel que $x=2^y$.\\
   
   On note ce réel $\log_2x$, ceci permet de définir la \textit{fonction logarithme binaire}.
   $$ y = \log_2x\quad\Longleftrightarrow\quad x = 2^y$$
\end{definition}

\begin{center}
   \begin{tikzpicture}[scale=14/21]
      \reperevl{-4}{-5}{17}{5}
      \draw[domain=1/32:17, smooth, samples = 500,variable=\x, thick, UGLiRed] plot ({\x}, {log2(\x))});
      \draw (.125,-3)\ball(.25,-2)\ball(.5,-1)\ball(1,0)\ball (2,1)\ball (4,2)\ball(16,4)\ball;
      \pointc{8}{3}{$8=2^3$}{$\log_28=3$}{}
   \end{tikzpicture}\\
   
   \textit{Courbe représentative de la fonction logarithme binaire}
\end{center}
La fonction logarithme binaire est très utile en informatique. Elle croît extrêmement lentement.
\begin{propriete}[]
   Soit $n\in\N^*$. On sait que si $$2^{p-1}\leqslant n< 2^p$$ alors $n$ s'écrit avec $p$ bits en binaire.   
\end{propriete}
\begin{encadrecolore}{Preuve}{gray}
   L'encadrement de la propriété est équivalent à $ 2^{p-1}\leqslant n\leqslant 2^p - 1$ car $n$ est un entier.\\ 
   Or $2^{p-1}=\left(1\underbrace{0\ldots 0}_{p-1\text{ zéros}}\right)_2$ s'écrit sur $p$ bits et 
   $2^{p}-1=\left(\underbrace{1\ldots 1}_{p\text{ uns}}\right)_2$ aussi.
\end{encadrecolore}

\begin{exemple}[]
   $256\leq 441<512$, c'est-à-dire $2^8\leq 441< 2^9$ et ainsi $441$ s'écrit avec 9 bits en binaire.\\
   D'ailleurs $441=(1\ 1011\ 1001)_2$.
   
\end{exemple}
On peut utiliser la fonction logarithme binaire pour retrouver le résultat de l'exemple précédent.


\begin{propriete}[]
   Soit $n\in\N$, le nombre de bits nécessaires pour écrire $n$ en binaire est $$\lfloor\log_2 n\rfloor +1$$
   
   $\lfloor\log_2 n\rfloor$ désigne la partie entière de $\log_2 n$.
\end{propriete}

\begin{exemple}[s]
   \begin{itemize}
      \item	$\log_2441 \simeq 8,7$, la partie entière de $8,7$ est $8$, augmentée de 1, cela nous donne 9 bits.
      \item	$\log_2131072 = 17$, augmenté de 1, cela nous donne 18 bits.
   \end{itemize}
\end{exemple}

\begin{methode}[ : Calculer un logarithme binaire]
   Pour calculer un logarithme binaire on peut se servir de la fonction \textit{logarithme népérien}, notée $\ln$. En effet pour tout réel strictement positif $x$ on a $$\log_2 x = \dfrac{\ln x}{\ln 2}$$
\end{methode}


\section{Des exemples de problèmes}

Voici quatre exemples de problèmes qu'on peut vouloir résoudre 

\subsection{Test de primalité}
Étant donné un entier $n$ plus grand que 1, cet entier est-il premier\footnote{\noindent est-ce qu'il existe d'autres diviseurs de $n$ que $1$ et $n$ ?} ou non ?\medskip

Par exemple, 127 est-il premier ? Pour y répondre, on va diviser 127 par 2, par 3, par 4, \textit{et cætera}, et regarder si une de ces divisions « tombe juste » ou non. Si c'est le cas, 127 n'est pas premier. On n'a pas besoin de pousser les divisions jusqu'à 126, il suffit simplement d'aller jusqu'à $\lfloor\sqrt{127}\rfloor$, c'est-à-dire onze\footnote{\noindent en effet, si $n$ admet un diviseur, alors on peut écrire $n=pq$ avec $p$ et $q$ deux entiers et il y en a obligatoirement un des deux qui est plus petit ou égal à $\sqrt{n}$, donc à $\lfloor\sqrt{n}\rfloor$ puisqu'il est entier.}.\\
Quand on le fait, on se rend compte que 127 est premier.

\subsection{Recherche de la présence d'un élément dans une liste}
Étant donnée une liste d'entiers \mintinline{python}{lst} de longueur $n$ et un entier \mintinline{python}{val}, cet entier appartient-il ou non à \mintinline{python}{lst} ?\medskip

Par exemple, avec une liste \mintinline{python}{lst} valant \mintinline{python}{[4, 2, 7, 8, 9]} et une valeur \mintinline{python}{val} de \mintinline{python}{5}, il faut parcourir \textit{intégralement} \mintinline{python}{lst} pour constater que \mintinline{python}{val} n'y figure pas.

\subsection{Table de multiplication}
Étant donné un entier $n\in\N^*$, on veut afficher tous les nombres de la forme $i\times j$, avec $i$ et $j$ entiers compris entre 1 et $n$.\medskip

Par exemple pour $n$ valant 4, j'obtiens la table suivante :
\begin{center}
   \tabstyled
   \begin{tabular}{c|c|c|c}
      1 & 2 & 3  & 4              \\
      2 & 4 & 6  & 8              \\
      3 & 6 & 9  & 12             \\
      4 & 8 & 12 & 16            
   \end{tabular}
\end{center}

\subsection{Nombre de bits nécessaires pour écrire un entier}

Étant donné un entier $n\in\N^*$, combien de bits sont nécessaires pour l'écrire en binaire ?\\

Pour $n=100$, il faut 7 bits, pour $n=255$, il en faut 8 et pour $n=256$, il en faut 9.
Pour $n=100$, il faut 7 bits, pour $n=255$, il en faut 8 et pour $n=256$, il en faut 9.\\



\section{Complexité temporelle}
Pour chacune des situations précédentes, $n$ est appelé \textit{taille du problème}. Plus $n$ augmente, plus le nombre d'opérations (au sens large : calculs, tests, accès aux éléments d'une liste, \textit{et cætera}) augmente.\\

À quelle vitesse ce nombre d'opérations augmente-t-il ?\\

S'il y a plusieurs algorithmes pour résoudre un même problème, y en a-t-il un plus efficace que les autres, c'est-à-dire dont le nombre d'opérations augmente moins vite lorsque $n$ augmente ?\\
Évaluer la complexité temporelle d'un algorithme, c'est estimer le nombre d'opérations \textit{significatives} qui entrent en jeu lors de la résolution par cet algorithme d'un problème de taille $n$. 

\begin{methode}[ : évaluation de la complexité temporelle]
   Pour évaluer l'efficacité d'un algorithme en terme de nombre d'opérations
   \begin{itemize}
      \item    d'abord on décide ce qu'est une opération significative. On appelle ceci une \textsc{opel} ;
      \item    seules les \textsc{opel} sont considérées comme coûteuses en temps et sont comptabilisées, les autres opérations sont \textit{négligées} ;
      \item    on cherche à estimer le nombre d'\textsc{opel} nécessaires à la résolution par l'algorithme d'un problème de taille $n$.
   \end{itemize}
\end{methode}


On peut « imaginer » une fonction $c_M$ (au sens mathématique du terme) qui serait définie pour toute taille $n$ du problème et	donnerait le nombre \textit{moyen}  d'\textsc{opel} nécessaires pour résoudre un problème de taille $n$.\\
Cette \textit{complexité moyenne} est très rarement calculable car les calculs trop compliqués.\\

Plus simple mais tout aussi utile que $c_M$, est la complexité \textit{dans le pire des cas}.

\begin{definition}[ : complexité dans le pire des cas]
   Pour un problème de taille $n$, on note $c(n)$ le nombre maximal d'\textsc{opel} pour résoudre un problème de cette taille.
\end{definition}

En pratique, on ne calcule pas \textit{exactement} $c(n)$. On se contente d'indiquer à quelle vitesse $c(n)$ augmente lorsque $n$ augmente, en se servant de \textit{fonctions de référence} telles que celles représentées ci-dessous.\\

\includegraphics[width=\linewidth]{img/complexite}\\

Il est très utile de connaître la complexité d'un algorithme car cela nous permet d'estimer le temps de resolution d'un problème quand on connaît l'ordre de grandeur de $n$ comme le montre le tableau ci-dessous.
\begin{center}

   \tabstyle[UGLiBlue]
   \renewcommand{\arraystretch}{1.5}
   \scriptsize
   \begin{tabular}{c|c|c|c|c|c|c|c|c}
      
      \cellcolor{white}    & {\ccell 5} & {\ccell 10} & {\ccell 20} & {\ccell 50} & {\ccell 250}  & {\ccell 1 000} & {\ccell 10 000} & {\ccell 1 000 000} \\\hline
      \ccell constante     & 10 ns      & 10 ns       & 10 ns       & 10 ns       & 10 ns         & 10 ns          & 10 ns           & 10 ns              \\\hline
      \ccell logarithmique & 10 ns      & 10 ns       & 10 ns       & 20 ns       & 30 ns         & 30 ns          & 40 ns           & 60 ns              \\\hline
      \ccell racinaire     & 22 ns      & 32 ns       & 45 ns       & 71 ns       & 158 ns        & 316 ns         & 1 µs            & 10 µs              \\\hline
      \ccell linéaire      & 50 ns      & 100 ns      & 200 ns      & 500 ns      & 2,5 µs        & 10 µs          & 100 µs          & 10 ms              \\\hline
      \ccell quadratique   & 250 ns     & 1 µs        & 4 µs        & 25 µs       & 625 µs        & 10 ms          & 1 s             & 2,8 h              \\\hline
      \ccell cubique       & 1,25 µs    & 10 µs       & 80 µs       & 1.25 ms     & 156 ms        & 10 s           & 2,7 h           & 316 ans            \\\hline
      \ccell exponentielle & 320 ns     & 10 µs       & 10 ms       & 130 jours   & $10^{59}$ ans & \ldots         & \ldots          & \ldots             \\
   \end{tabular}
   \renewcommand{\arraystretch}{1}
\end{center}


\section{Application à nos exemples}

\subsection{Test de primalité}

\begin{pyc}
   \begin{minted}{python}
      from math import sqrt
      def est_premier(n: int) -> bool:
         # on parcourt les entiers de 2 à racine(n)
         for i in range(2, int(sqrt(n)) + 1):
            # si une division tombe juste
            if n % i == 0:
               # alors n n'est pas premier
               return False
         # si aucune ne tombe juste il est premier
         return True
   \end{minted}
\end{pyc}

On convient qu'une \textsc{opel} est une division. Pour $n$ fixé, on effectue au pire des cas les divisions par 2, 3, \ldots, $\lfloor\sqrt{n}\rfloor$, il y en a \textit{grosso modo} $\sqrt{n}$ : notre algorithme est de complexité racinaire\footnote{en réalité c'est sans doute plus que cela car plus $n$ est grand, plus les nombres qui entrent en jeu dans les divisions sont grands et plus les divisions prennent de temps.}. 

\begin{center}
   \includegraphics[width=12cm]{img/primalite.pdf}
\end{center}

\subsection{Recherche de la présence d'un élément dans une liste}

\begin{pyc}
   \begin{minted}{python}
      def present(lst: list, val: int) -> bool:
         # on parcourt la liste par ses éléments
         for x in lst:
            # si on trouve val
            if x == val:
               # on renvoie True
               return True
         # si on ne l'a pas trouvé, on renvoie False
         return False
   \end{minted}
\end{pyc}
Ici une \textsc{opel} sera un accès à un élément de la liste, c'est à dire le fait d'attribuer les valeurs à la variable \mintinline{python}{x}. Pour une liste de taille $n$ fixé, dans le pire des cas \mintinline{python}{val} n'appartient pas à \mintinline{python}{lst} et on effectue $n$ \textsc{opel} : notre algorithme est de complexité linéaire.

\begin{center}
   \includegraphics[width=12cm]{img/recherche.pdf}
\end{center}

\begin{pyc}
   \begin{minted}{python}
      def affiche(n: int) -> None:
         for i in range(1, n + 1):
            for j in range(1, n + 1):
               print(i * j)
   \end{minted}
\end{pyc}
\subsection{Table de multiplication}
Ici une \textsc{opel} est une multiplication. Pour $n$ fixé, dans tous les cas on effectue deux boucles imbriquées, donc $n\times n$ \textsc{opel} sont effectuées et notre algorithme est de complexité quadratique.
\begin{center}
   \includegraphics[width=12cm]{img/multiplication.pdf}
\end{center}

\subsection{Nombre de bits nécessaires pour écrire un entier}

\begin{pyc}
   \begin{minted}{python}
      def nb_bits(n: int) -> int:
         k = 0
         while 2 ** k <= n:
            k += 1
         return k
   \end{minted}
\end{pyc}

Ici une \textsc{opel} est une multiplication par 2 et pour $n$ fixé, le nombre d'\textsc{opel} est exactement le plus petit entier $k$ tel que $2^k > n$, ce qui revient à dire que $k$ vaut $\lfloor \log_2 n \rfloor +1$. Notre algorithme est donc de complexité \textit{logarithmique}.
\begin{center}
   \includegraphics[width=12cm]{img/puissance.pdf}
\end{center}


\section{Quelques résultats utiles}
\subsection*{Sommes d'entiers consécutifs}
\begin{propriete}[]
   $\forall n\in\N$, on a $\displaystyle 0 + 1 + \ldots + n = \dfrac{n(n+1)}{2}$.
\end{propriete}
Cela se prouve par récurrence sur $n$, ou alors par un dessin\dots

\begin{propriete}[ (qui découle de la première)]
   $\forall p\in\N$, $\forall n\in\N$ tels que $p\leqslant n$ , on a $\displaystyle p + 1 + \ldots + n = \dfrac{(n-p+1)(p+n)}{2}$. \\

C'est le nombre de termes qu'on ajoute multiplié par la moyenne du premier et du dernier terme de la somme.
\end{propriete}
Cela se prouve en écrivant que le membre de gauche vaut $0+1+\ldots+n - \left(1+\ldots+p-1\right)$, en remplaçant ces deux sommes par leur expression suivant la propriété précédente et en réduisant.

\begin{propriete}[]
   Si \mintinline{python}{a} et \mintinline{python}{b} sont deux \mintinline{python}{int} avec \mintinline{python}{a < b} alors une boucle\\ \mintinline{python}{for i in range(a, b)} est exécutée \mintinline{python}{b-a} fois.  
\end{propriete}
En effet, \mintinline{python}{i} prend les valeurs \mintinline{python}{a}, \mintinline{python}{a + 1},\ldots, \mintinline{python}{b - 1}, et il y en a \mintinline{python}{b - 1 - a + 1}. 
\section{Exercices}
\begin{exercice}
   Une \textsc{opel} est une addition, déterminer la complexité de la fonction suivante:
   \begin{minted}{python}
   def somme(n: int) -> int:
      s = 0
      for i in range(1, n + 1):
         s += i
      return s
    \end{minted}
\end{exercice}

\begin{exercice}[ : valeurs d'une liste les plus proches]
   La fonction suivante 
   \begin{itemize}
      \item prend en entrée une liste \mintinline{python}{lst} d'au moins 2 entiers ;
      \item renvoie les 2 valeurs de cette liste qui sont les plus proches.
   \end{itemize}
   
   Par exemple \mintinline{python}{plus_rapproches([-4,0,2,5,7,-3])} vaut \mintinline{python}{(-4,-3)} car ce sont les 2 valeurs de la liste qui sont les plus proches.
   \begin{minted}{python}
   def plus_rapproches(lst: list) -> tuple:
    mini = float('inf')
    for i in range(len(lst)):
        for j in range(i + 1, len(lst)):
            a, b = lst[i], lst[j]
            d = abs(a - b)
            if d < mini:
                mini = d
                result = a, b
    return result
   \end{minted}
   Une \textsc{opel} est un accès à tout élément de la liste. Quelle est la complexité de cette fonction ? 
   
\end{exercice}

\begin{exercice}[* : addition de matrices]
   On considère des \textit{matrices}, qui sont représentées par des listes de listes d'\mintinline{python}{int} ($n$ lignes et $n$ colonnes).
   Par exemple les matrices 
   \tabulardefault
   $$M_1 = \begin{matrice}
         1  & 2  & -1 \\
         0  & -1 & 2  \\
         -1 & 1  & 0  \\
      \end{matrice}\quad\text{et}\quad M_2 = \begin{matrice}
         -3 & 1  & 0  \\
         1  & 0  & -2 \\
         1  & -1 & 1  \\
      \end{matrice}$$
   sont deux matrices carrées d'ordre 3 et sont représentées en \textsc{Python} par \\
   \mintinline{python}{m1 = [[1, 2, -1], [0, -1, 2], [-1, 1, 0]]} et\\
   \mintinline{python}{m2 = [[-3, 1, 0], [1, 0, -2], [1, -1, 1]]}\\
   
   Il est possible d'ajouter 2 matrices de même taille en procédant « case par case »  :
   $$M_1+M_2 = \begin{matrice}
         -2 & 3  & -1 \\
         1  & -1 & 0  \\
         0  & 0  & 1  \\
      \end{matrice}$$
   En convenant qu'une \textsc{opel} est une addition de deux \mintinline{python}{int} , sans écrire l'algorithme, donner la complexité de l'algorithme d'addition de deux matrices.
\end{exercice}
\begin{exercice}[* : multiplication de matrices]
   
   
   Soient $A$ et $B$ deux matrices carrées d'ordre $n$.\\
   $C=A\times B$, produit de $A$ par $B$, est la matrice carrée d'ordre $n$ dont les coefficients sont ainsi :
   \begin{center}
      \begin{tabular}{cc}
                                                                                                                                                                         & $\begin{matrice}
                                                                                                                                                                                  b_{11} & \cdots & \cellcolor{lightgray!50}\color{UGLiOrange}b_{1j} & \cdots & a_{1n} \\
                                                                                                                                                                                  \vdots & \ddots & \cellcolor{lightgray!50}\color{UGLiOrange}\cdots & \dots  & \vdots \\
                                                                                                                                                                                  b_{k1} & \cdots & \cellcolor{lightgray!50}\color{UGLiOrange}b_{kj} & \cdots & b_{kn} \\
                                                                                                                                                                                  \vdots & \cdots & \cellcolor{lightgray!50}\color{UGLiOrange}\cdots & \ddots & \vdots \\
                                                                                                                                                                                  b_{n1} & \cdots & \cellcolor{lightgray!50}\color{UGLiOrange}b_{pj} & \cdots & b_{nn}
                                                                                                                                                                               \end{matrice}$ \\
         
         $\begin{matrice}
                a_{11}                                              & \cdots                  & \cdots                   & \cdots                  & a_{1n}                  \\
                \vdots                                              & \ddots                  & \cdots                   & \dots                   & \vdots                  \\
                \rowcolor{lightgray!50} \color{UGLiGreen}    a_{i1} & \color{UGLiGreen}\cdots & \color{UGLiGreen} a_{ik} & \color{UGLiGreen}\cdots & \color{UGLiGreen}a_{ip} \\
                \rowcolor{white}\vdots                              & \cdots                  & \cdots                   & \ddots                  & \vdots                  \\
                a_{n1}                                              & \cdots                  & \cdots                   & \cdots                  & a_{nn}
             \end{matrice}$ & 
         $\begin{matrice}
                c_{11} & \cdots & \cdots                                         & \cdots & c_{1n} \\
                \vdots & \ddots & \cdots                                         & \dots  & \vdots \\
                \vdots & \cdots & \cellcolor{lightgray!50} \color{UGLiRed}c_{ij} & \cdots & \vdots \\
                \vdots & \cdots & \cdots                                         & \ddots & \vdots \\
                c_{n1} & \cdots & \cdots                                         & \cdots & c_{nn}
             \end{matrice}$
      \end{tabular}
   \end{center}
   {\LARGE$$ {\color{UGLiRed}c_{ij}} = {\color{UGLiGreen}a_{i1}}\times {\color{UGLiOrange}b_{1j}}+ {\color{UGLiGreen}a_{i2}}\times {\color{UGLiOrange}b_{2j}}+\ldots+ {\color{UGLiGreen}a_{ip}}\times {\color{UGLiOrange}b_{pj}}$$}
   
   
   
   Cela donne lieu à l'algorithme suivant :
   
   \begin{minted}{python}
         def produit(a: list, b: list) -> list:
            
            # n est la taille des matrices
            n = len(a) 

            # on crée une matrice remplie de zéros
            c = [[0 for j in range(n)] for i in range(n)]

            # pour chaque ligne
            for i in range(n):
              
              # pour chaque colonne
               for j in range(n):

                  # on ajoute tous ces nombres
                  for k in range(n):
                     c[i][j] += a[i][k] * b[k][j]

            # puis on renvoie la matrice produit
            return c
      \end{minted}
   
   
   Déterminer la complexité de cet algorithme (une \textsc{opel} est une multiplication).
\end{exercice}
\end{document}