from math import log2

def nb_bits(n: int) -> int:
    k = 0
    while 2 ** k <= n:
        k += 1
    return k


a = 2
print(nb_bits(a))
print(int(log2(a))+1)
