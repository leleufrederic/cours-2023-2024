\documentclass[10pt,firamath,cours]{nsi}
\begin{document}
\setcounter{chapter}{19}
\chapter{Protocoles de routage}

\section{Situation}
Lors d'une communication \emph{via} un réseau, nous allons distinguer plusieurs acteurs :
\begin{itemize}
    \item une machine appelée \emph{client} doit envoyer une information à une
          autre machine ;
    \item cette deuxième machine s'appelle un \emph{serveur}.
\end{itemize}
Les termes \emph{client} et \emph{serveur} sont très larges et peuvent
en fait
\begin{itemize}
    \item désigner une \emph{machine} aussi bien qu'une \emph{application}
          s'exécutant sur cette machine ;
    \item alterner au cours du temps (le client devient serveur et vice-versa).
\end{itemize}
Les autres acteurs de la communication sont les \emph{routeurs} : on a vu en classe de première que lors de la communication l'information est découpée en de multiples paquets de petite taille.
Ces paquets doivent arriver à destination et ce sont les routeurs qui acheminent les paquets au sein des réseaux.

\section{Les routeurs}
Ils peuvent être de deux types :
\begin{itemize}
    \item \emph{routeurs d'accès} lorsqu'ils sont en \emph{bordure de réseau},
          c'est-à-dire qu'ils sont directement interfacés avec un réseau
          \emph{local} ;
    \item \emph{internes} sinon.
\end{itemize}
Voici un schéma qui montre la \emph{topologie} d'un réseau, c'est-à-dire
son architecture.
\begin{center}
    \includegraphics[width=\textwidth]{img/topo1}\\
    \emph{un exemple de réseau}
\end{center}
Le réseau comprenant R1 et R3 a pour adresse 10.0.1.0/30 : il ne reste
donc que 2 bits libres pour adresser les machines, soit 4 possibilités.

Si on enlève l'adresse réseau 10.0.1.0 et l'adresse \emph{broadcast}
10.0.1.3 il reste 2 IP, une pour chaque routeur.

Ainsi par exemple
\begin{itemize}
    \item R1 peut avoir l'IP 10.0.1.1 ;
    \item R3 aura dans ce cas l'IP 10.0.1.2 ;
    \item ou l'inverse.
\end{itemize}
R1 est un routeur \emph{externe} :
\begin{itemize}
    \item il a aussi une IP dans le réseau local du client et réalise ainsi une
          \emph{passerelle} ;
    \item c'est cette IP que le client utilise pour envoyer des informations à
          l'extérieur de son réseau local.
\end{itemize}
R3 est un routeur \emph{interne} :
\begin{itemize}
    \item il a 4 IP différentes sur les 4 réseaux auxquels il est connecté.
\end{itemize}

\section{Routage des paquets}

Lorsqu'un paquet doit transiter du client au serveur
\begin{itemize}
    \item il doit obligatoirement passer la passerelle R1 ;
    \item là encore il n'y a pas le choix, il passera par R3.
\end{itemize}
Mais ensuite ?\\

Comment la route à emprunter est-elle déterminée ? Est-ce la même tout le temps ?\\

En fait, chaque routeur possède une \emph{table de routage} qui associe
les IP de destination à des routeurs particuliers.
Ces tables ne sont pas fixes.
\emph{A priori} tous les routeurs ont le même statut (il n'y a pas de
routeur privilégié).
Les méthodes qui permettent de gérer ces tables de routage sont appelés
des \emph{protocoles de routage}.


\section{Le protocole RIP}

RIP signifie \emph{Routing Information Protocol}.

\subsection{Principe}
À intervalles de temps réguliers, chaque routeur envoie à ses voisins
\begin{itemize}
    \item les adresses réseau de ses propres routeurs voisins ;
    \item les adresses qu'il a reçues par d'autres routeurs.
\end{itemize}
Pour chaque adresse, il indique également combien de sauts sont
nécessaires pour l'atteindre, c'est-à-dire par combien de routeurs (y
compris lui-même) il faut passer.

Lorsqu'un routeur reçoit les informations d'un routeur voisin, 4 cas
peuvent survenir :

\begin{enumerate}  
    \item  Une route vers un nouveau sous-réseau lui est présentée : il l'ajoute
          à sa table de routage.
    \item Une route vers un sous-réseau déjà connu lui est présentée, mais plus
          courte que la précédente. Dans ce cas l'ancienne est remplacée par
          celle-ci.
    \item Une nouvelle route vers un sous-réseau plus longue lui est transmise
          par un autre voisin que celui de l'ancienne route : il l'ignore.
    \item Une route existante, passant par le même voisin, mais plus longue que
          celle de la table de routage lui est présentée. Cela veut dire qu'un
          problème est survenu sur l'ancienne route. Celle-ci est donc effacée
          et remplacée par la plus longue.
\end{enumerate}

Pour éviter les boucles, les distances doivent être au maximum de 15
(sinon elles sont ignorées).

RIP fonctionne donc sur des réseaux de taille modeste.

\subsection{Exemple}

Reprenons le réseau précédent et intéressons-nous uniquement aux
routeurs R1 et R3.

\subsubsection{Étape 1 :
    initialisation}

Au début de la mise en service du réseau voici la table de routage de R1 :
\begin{center}
    \tabstyle[UGLiGreen]
    \begin{tabular}{c|c|c|c}
        \ccell destination & \ccell passerelle & \ccell interface & \ccell distance \\
        10.0.1.0/30        &                   & eth0             & 1               \\
        192.168.1.0/24     &                   & wlan0            & 1               \\
    \end{tabular}
\end{center}
Elle indique que le sous-réseau local 192.168.1.0/24 est immédiatement
accessible \emph{via} l'interface \emph{WiFi} wlan0 depuis ce propre
routeur R1. Elle est donc à distance 1 de R1.

De même l'autre sous-réseau est accessible \emph{via} un port
\emph{Ethernet} du routeur nommé eth0 et est également à distance 1 de
R1.

Voici celle de R3 :
\begin{center}\tabstyle[UGLiGreen]
    \begin{tabular}{c|c|c|c}
        \ccell destination & \ccell  passerelle & \ccell  interface & \ccell  distance \\
        10.1.1.0/30        &                    & eth1              & 1                \\
        10.1.2.0/30        &                    & eth2              & 1                \\
        10.1.3.0/30        &                    & eth0              & 1                \\
        10.1.4.0/30        &                    & eth4              & 1                \\
    \end{tabular}
\end{center}

C'est la même chose : R3 est initialisé avec ses voisins directs.

Les noms des interfaces sont relatifs à R3.

R1 et R3 sont reliés par \emph{Ethernet} sur le port eth0 de R1 et eth1
de R3. Ces ports peuvent avoir le même nom ou pas, peu importe, car ces
noms n'existent que relativement au routeur concerné.

\subsubsection{Étape 2 : première itération de
    RIP}

Chaque routeur envoie ses informations à ses voisins. La table de R1
change :

\begin{center}\tabstyle[UGLiGreen]
    \begin{tabular}[]{c|c|c|c}
        \ccell destination & \ccell passerelle & \ccell interface & \ccell distance \\
        10.0.1.0/30        &                   & eth0             & 1               \\
        192.168.1.0/24     &                   & wlan0            & 1               \\
        10.0.2.0/30        & 10.1.1.2          & eth0             & 2               \\
        10.0.3.0/30        & 10.1.1.2          & eth0             & 2               \\
        10.0.4.0/30        & 10.1.1.2          & eth0             & 2               \\
    \end{tabular}
\end{center}

R1 sait maintenant qu'il peut atteindre les machines du sous-réseau
10.1.2.0/30 \emph{via} la passerelle 10.1.1.2 (IP de R2) sur le
sous-réseau 10.1.1.0/30.

L'interface est eth0 et la distance est 2.

La table de R3 change aussi :
\begin{center}\tabstyle[UGLiGreen]
    \begin{tabular}{c|c|c|c}
        \ccell destination & \ccell passerelle & \ccell interface & \ccell distance \\ 
        10.1.1.0/30        &                   & eth1             & 1               \\
        192.168.1.0/24     & 10.1.1.1          & eth1             & 2               \\
        10.1.2.0/30        &                   & eth2             & 1               \\
        10.1.3.0/30        &                   & eth0             & 1               \\
        10.1.4.0/30        &                   & eth3             & 1               \\
        10.1.7.0/30        & 10.1.4.2          & eth3             & 2               \\
    \end{tabular}
\end{center}
\subsubsection{Étape 3 : convergence après quelques itérations}
Dans notre cas, après 2 autres itérations, les informations se
stabilisent.

On dit qu'il y a \emph{convergence}.

Chaque routeur connaît le chemin à emprunter pour accéder à n'importe
quel sous-réseau.

Table de R1 « stabilisée »

\begin{center}\tabstyle[UGLiGreen]
    \begin{tabular}{c|c|c|c}
        \ccell destination & \ccell passerelle & \ccell interface & \ccell distance \\
        10.0.1.0/30        &                   & eth0             & 1               \\
        192.168.1.0/24     &                   & wlan0            & 1               \\
        10.0.2.0/30        & 10.1.1.2          & eth0             & 2               \\
        10.0.3.0/30        & 10.1.1.2          & eth0             & 2               \\
        10.0.4.0/30        & 10.1.1.2          & eth0             & 2               \\
        10.0.5.0/30        & 10.1.1.2          & eth0             & 3               \\
        10.0.6.0/30        & 10.1.1.2          & eth0             & 3               \\
        10.0.7.0/30        & 10.1.1.2          & eth0             & 3               \\
        192.162.6.0/24     & 10.1.1.2          & eth0             & 4               \\
    \end{tabular}
\end{center}

Le protocole RIP fait suivre des routes minimisant le nombre de sauts.

Cela peut être une très mauvaise idée !

\begin{center}
    \includegraphics[width=10cm]{img/rip_nase}\\
    \emph{problème avec RIP}
\end{center}

Il faut prendre en compte le débit des interconnexions.

C'est ce que fait le protocole OSPF.




\section{Protocole OSPF}

Cela signifie \emph{Open Shortest Path First}

Le principe est simple : choisir la route la plus « rapide ».

\begin{itemize}
    \item Selon ce protocole, les routeurs ont une vision globale d'une
          \emph{aire} (réseau entier s'il est de taille modeste, portion de ce
          réseau sinon).
    \item Chaque routeur connaît donc la \emph{topologie du réseau} et les
          bandes passantes des connexions entre routeur.
\end{itemize}

\begin{center}
    \includegraphics[width=7cm]{img/ospf2}\\
    \emph{exemple de réseau}
\end{center}

À chaque liaison on associe un \emph{coût} :

\begin{itemize}
    \item le coût d'une liaison est inversement proportionnel à la bande
          passante ;
    \item en général il est de \(\dfrac{10^8}{d}\), où \(d\) est la bande
          passante de la liaison en bits/s.
\end{itemize}

\begin{center}
    \includegraphics[width=7cm]{img/ospf3}\\
    \emph{réseau avec coûts}
\end{center}

Chaque routeur applique l'algorithme de Dijkstra pour déterminer les
chemins de coûts minimaux.
Cet algorithme est hors programme mais on peut déterminer les chemins de coûts minimaux à la
main.

\begin{center}
    \includegraphics[width=7cm]{img/ospf4}\\
    \emph{chemin de coût minimal entre A et G}
\end{center}

\section{Exercices}

\begin{center}
    \emph{Pour tous ces exercices, si on a le choix, on préférera prendre le routeur dont le numéro est le plus petit ou dont la lettre est la première dans l'alphabet.}
\end{center}
\begin{exercice}[ : Convergence du protocole RIP]
    On considère le réseau suivant
    \begin{center}
        \includegraphics[width=6cm]{img/ex_res1.png}
    \end{center}
    
    Donner étape par étape l'évolution des tables de routage de R1 et R6.\\
    On n'indique pas les IP, juste les routeurs (c'est souvent ainsi lors du baccalauréat), par exemple au départ, R2 est à distance 1 de R1 en passant par... R1.
\end{exercice}

\begin{exercice}[ : Effet d'une panne]
    On considère le réseau suivant
    \begin{center}
        \includegraphics[width=3cm]{img/ex_res2.png}
    \end{center}
    
    \newpage 
    
    \begin{enumerate}
        \item Remplir la table de routage de A après convergence de RIP.
              \begin{center}
                  \tabstyle[UGLiOrange]
                  \begin{tabular}{|c|c|c|}
                      \hline
                      \ccell Destination & \ccell Distance & \ccell Intermédiaire \\	
                      \hline
                      B                  &                 &                      \\
                      \hline
                      C                  &                 &                      \\
                      \hline
                      D                  &                 &                      \\
                      \hline
                      E                  &                 &                      \\
                      \hline
                      F                  &                 &                      \\
                      \hline
                  \end{tabular}
              \end{center}
        \item Le routeur B tombe en panne : il n'émet plus de message RIP et est donc au bout d'un certain temps, enlevé des tables des autres routeurs.\\
              Quelle sera la nouvelle table de routage de A ?
    \end{enumerate}
\end{exercice}

\begin{exercice}[ : Bande passante, coût OSPF]
    
    Le coût OSPF d'une liaison réseau est $\dfrac{10^8}{d}$ où $d$ est son débit en bits/s.\\
    Complète le tableau suivant 
    \begin{center}
        \tabstyle[UGLiOrange]
        \begin{tabular}{|c|c|c|}
            \hline
            \ccell Débit & \ccell puissance de 10 & \ccell coût \\
            \hline
            1 Mbit/s     & $10^6$                 &             \\
            \hline
            10 Mbits/s   &                        &             \\
            \hline
            100 Mbits/s  &                        &             \\
            \hline
            1 Gbit/s     &                        &             \\
            \hline
            10 Gbit/s    &                        &             \\
            \hline
        \end{tabular}
    \end{center}
\end{exercice}


\begin{exercice}[ : Comparaison RIP/OSPF]
    On 	considère le réseau suivant 
    \begin{center}
        \includegraphics[width=14cm]{img/ex_rip}
    \end{center}
    \begin{enumerate}
        \item	Si les routeurs suivent le protocole RIP, quel est la route empruntée par un paquet acheminé de R1 vers R7 ? Justifier.
        \item	Les routeurs sont maintenant configurés suivant le protocole OSPF. Les débits des interconnexions sont reportés ci-dessous :
              \begin{center}
                  \includegraphics[width=14cm]{img/ex_ospf}
              \end{center}
              \begin{enumalph}
                  \item 	Reporter sur le graphe les coûts des connexions.
                  \item 	On veut connaître le chemin emprunté par un paquet transitant de R1 vers R7. Indiquer les coûts de chaque route et conclure.
                  
                  \item Quel est le coût de la route choisie par le protocole RIP ?
              \end{enumalph}
    \end{enumerate}
\end{exercice}
\end{document}