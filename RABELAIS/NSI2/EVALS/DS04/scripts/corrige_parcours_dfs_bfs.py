import networkx as nx


G = nx.Graph()


G.add_edges_from([('A', 'E'),

                   ('E', 'R'), ('E', 'Z'),

                   ('R', 'T'),

                   ('T', 'Y'), ('T','Q'),('T', 'Z'),

                   ('U', 'Y'),

                   ('P', 'Y'),

                   ('O', 'P'),

                   ('I', 'O'), ('I', 'Y'), ('I', 'Q')
                  ])



def dfs(g: nx.Graph, start):

    path = []

    remaining = list(g.nodes)

    stack = []

    stack.append(start)
    remaining.remove(start)

    while stack:

        current = stack.pop()

        path.append(current)

        for v in sorted(list(g.neighbors(current))):

            if v in remaining:

                stack.append(v)
                remaining.remove(v)

    return path


def dfs_rev(g: nx.Graph, start):

    path = []

    remaining = list(g.nodes)

    stack = []

    stack.append(start)
    remaining.remove(start)

    while stack:

        current = stack.pop()

        path.append(current)

        for v in sorted(list(g.neighbors(current)),reverse=True):

            if v in remaining:

                stack.append(v)
                remaining.remove(v)

    return path


def bfs(g: nx.Graph, start):

    path = []

    remaining = list(g.nodes)

    stack = []

    stack.append(start)
    remaining.remove(start)

    while stack:

        current = stack.pop(0)

        path.append(current)

        for v in sorted(list(g.neighbors(current))):

            if v in remaining:

                stack.append(v)
                remaining.remove(v)

    return path


def bfs_rev(g: nx.Graph, start):

    path = []

    remaining = list(g.nodes)

    stack = []

    stack.append(start)
    remaining.remove(start)

    while stack:

        current = stack.pop(0)

        path.append(current)

        for v in sorted(list(g.neighbors(current)),reverse=True):

            if v in remaining:

                stack.append(v)
                remaining.remove(v)

    return path


print(dfs(G, 'U'))

print(bfs(G, 'I'))



