\documentclass[a4paper,12pt,eval,firamath]{nsi}
\titre{Contrôle 04}
\classe{NSI2}
\begin{document}
\maketitle

\begin{encadrecolore}{Nota bene}{UGLiOrange}
Pour toute cette évaluation
\begin{itemize}
    \item on ne s'intéresse qu'aux graphes \textit{non orientés};
    \item on convient qu'une \textsc{opel} est un accès en lecture ou en écriture à un élément d'une liste, à la clé d'un dictionnaire ou à la valeur associée à cette clé.
    \item la complexité désigne le nombre d'\textsc{opel} nécessaires pour effectuer un algorithme avec une donnée de taille $n$.
    \item Avec les versions récentes de Python, \textbf{le parcours d'un dictionnaire se fait dans l'ordre de création des clés}.
\end{itemize}
\end{encadrecolore}

\section*{Amuse-gueule}
\resetquestion
\begin{center}
    \includegraphics[width=6cm]{img/graphe_parcours.png}
\end{center}

Par souci de simplicité, on veillera à empiler/enfiler les sommets dans l'ordre alphabétique.\\

\question Indiquer la liste obtenue lors d'un parcours en profondeur de ce graphe en commençant par U.\bareme{2}\\
%['U', 'Y', 'T', 'Z', 'E', 'A', 'R', 'Q', 'P', 'O', 'I']

\carreauxseyes{16.8}{1.6}\\

\question Indiquer maintenant la liste obtenue lors d'un parcours en largeur de ce graphe en commençant par I.\bareme{2}\\
%['I', 'O', 'Q', 'Y', 'P', 'T', 'U', 'R', 'Z', 'E', 'A']

\carreauxseyes{16.8}{1.6}\\
\resetquestion
\section*{Implémentations des graphes non orientés}
\subsection*{À l'aide de la matrice d'adjacence}
On considère un graphe $G$ \textit{non orienté} dont la matrice d’adjacence est implémentée en \textsc{python} par une liste de listes :
\begin{minted}{python}
M = [[0, 1, 0, 1]
     [1, 0, 1, 0]
     [0, 1, 0, 1]
     [1, 0, 1, 0]]
\end{minted}

\question{}En appelant ses sommets A, B, C et D, dessiner le graphe $G$.\bareme{1}\\

\begin{tikzpicture}
\draw (0,0) rectangle (17,4);
\end{tikzpicture}\\

\question Écrire une fonction \mintinline{python}{voisins_mat} qui\bareme{2}
\begin{itemize}
	\item en entrée prend 
	\begin{itemize}
		\item une liste de listes d'entiers \mintinline{python}{lst}, matrice d'adjacence d'un graphe non orienté ;
		\item un entier \mintinline{python}{k}, indice d'un des sommets du graphe.
	\end{itemize}
	\item renvoie la liste des indices des sommets voisins de celui d'indice k.
\end{itemize}
Par exemple, \mintinline{python}{voisins_mat(M, 2)} renvoie \mintinline{python}{[1, 3]}.\\

\carreauxseyes{16.8}{6.4}\\

\question Quelle est la complexité de cette fonction ?\bareme{1}\\

\carreauxseyes{16.8}{3.2}\\

\question Combien d'éléments figurent dans la matrice d'adjacence d'un graphe non orienté à 1000 sommets ?\bareme{1}\\

\carreauxseyes{16.8}{2.4}\\

\subsection*{À l'aide d'un dictionnaire}
On peut également implémenter la structure de graphe non orienté grâce à un dictionnaire dont les clefs sont les sommets du graphe et les valeurs sont des listes contenant les voisins du sommet considéré.\\

\question Donner le dictionnaire \mintinline{python}{g1} associé au graphe $G_1$ représenté ci-dessous, .\bareme{1}
\begin{center}
\includegraphics[width=7cm]{img/graphe_1.png}
\end{center}

\carreauxseyes{16.8}{4.8}\\

\question Écrire une fonction \mintinline{python}{voisins_dict} qui \bareme{2}
\begin{itemize}
    \item en entrée prend 
    \begin{itemize}
        \item un dictionnaire \mintinline{python}{g} dont les clés sont les sommets d'un graphe non orienté et les valeurs les listes des voisins de chaque clé ;
        \item un \mintinline{python}{str} \mintinline{python}{s} qui est un sommet du graphe.
    \end{itemize}
    \item renvoie la liste des sommets voisins de \mintinline{python}{s}.\\
\end{itemize}
\carreauxseyes{16.8}{4.8}\\

\question Quelle est la complexité de cette fonction ?\bareme{1}\\

\carreauxseyes{16.8}{2.4}\\

\question Combien d'éléments y a-t-il dans le dictionnaire si le graphe est de taille 1000 ?\bareme{1}\\

\carreauxseyes{16.8}{2.4}\\



\section*{Coloration de graphes non orientés}

\begin{encadrecolore}{Nota Bene}{UGLiOrange}
 Les graphes considérés seront désormais \textit{sans boucles}.\\
On utilisera la deuxième implémentation : un graphe est un « dictionnaire de voisins ».\\

On considère un graphe.
On veut colorer ses sommets 
\begin{itemize}
    \item de telle manière qu'aucun sommet n'ait la même couleur que ses voisins ;
    \item en utilisant le moins de couleurs possibles.
\end{itemize}
Pour représenter les couleurs, on utilisera des entiers.\\
Dorénavant, par \textit{coloration de graphe}, on sous-entend que c'est de cette manière.\\
\end{encadrecolore}

\begin{center}
\includegraphics[width=5cm]{img/2_graphes}\\ \textit{2 colorations de graphes}
\end{center}
\question Colore les graphes suivants (utilise des entiers).\bareme{1}
\begin{center}
\includegraphics[width=10cm]{img/3_graphes}
\end{center}
\question Dans le pire des cas, pour un graphe à $n$ sommets, combien faut-il de couleurs ?\bareme{1}\\

\carreauxseyes{16.8}{4}

\subsection*{Coloration séquentielle}

Une première approche pour colorer un graphe est de prendre ses sommets les uns après les autres \textit{dans n'importe quel ordre} afin de leur affecter une couleur, tout en veillant à ce que deux sommets adjacents n’aient jamais la même couleur : c’est l’algorithme de \textit{coloration séquentielle}.\\

On propose le code ci-dessous qui utilise la fonction \mintinline{python}{voisins_dict} programmée à la question précédente.
\newpage
\begin{minted}[fontsize=\small,linenos=true]{python} 
def coloration(g: dict) -> dict:
    liste_couleurs = [i for i in range(len(g))]
    couleur_sommet = {sommet: None for sommet in g.keys()}
    for sommet in g.keys():
        voisins = voisins_dict(g, sommet)
        couleurs_prises = [couleur_sommet[s] for s in voisins]
        k = 0
        while liste_couleurs[k] in couleurs_prises:
            k = k + 1
        couleur_sommet[sommet] = liste_couleurs[k]
    return couleur_sommet
\end{minted}

\question Explique le lien entre la ligne 2 et la question précédente.\bareme{1}\\

\carreauxseyes{16.8}{2.4}\\

\question Donne les couleurs attribuées par \mintinline{python}{coloration} aux sommets de $G_1$, graphe de la question \textbf{5.} ?\bareme{1}\\

\carreauxseyes{16.8}{2.4}\\

\question Explique pourquoi l'algorithme de \mintinline{python}{coloration} est glouton.\bareme{2}\\

\carreauxseyes{16.8}{4}\\

On évalue maintenant la fonction \mintinline{python}{coloration} sur le graphe suivant :
\begin{minted}{python}
>>> g2 = {'A' : ['C'], 'B' : ['D'],
          'C' : ['A','D'], 'D' : ['B','C']}

>>> print(coloration(g2))

{'A': 0, 'B': 0, 'C': 1, 'D': 2}
\end{minted}

\question Dessiner le graphe $G_2$ et trouver une coloration avec moins de couleurs.\bareme{2}\\

\begin{tikzpicture}
\draw (0,0) rectangle (17,4);
\end{tikzpicture}

\subsection*{Méthode de Welsh-Powell}

L'idée est de faire de la coloration séquentielle en commençant par les sommets les plus délicats : ceux qui ont le plus de voisins.

\begin{definition}[ : Degré d'un sommet d'un graphe non orienté]
C'est le nombre d'arêtes partant du sommet. Quand le graphe est sans boucles c'est le nombre de ses voisins.
\end{definition}

\question Écrire une fonction \mintinline{python}{degre} qui\bareme{2}
\begin{itemize}
\item en entrée prend 
    \begin{itemize}
        \item un dictionnaire \mintinline{python}{g} dont les clés sont les sommets d'un graphe non orienté et les valeurs les listes des voisins de chaque clé ;
        \item un \mintinline{python}{str} \mintinline{python}{s} qui est un sommet du graphe.
    \end{itemize}
    \item renvoie le degré de \mintinline{python}{s}.\\
\end{itemize}

\carreauxseyes{16.8}{4}\\

\question Compléter la fonction \mintinline{python}{tri_sommets} qui \bareme{2}
\begin{itemize}
\item en entrée prend un dictionnaire \mintinline{python}{g} dont les clés sont les sommets d'un graphe non orienté et les valeurs les listes des voisins de chaque clé ;
    \item renvoie la liste des sommets de \mintinline{python}{g} dans l'ordre des degrés décroissants.
\end{itemize}

\begin{minted}[fontsize=\small]{python}
def tri_sommets(g: dict) -> list:
    sommets = [s for s in g.keys()]
    n = len(sommets)
    for i in range(n - 1):
        indice_max = i
        degre_max = ...
        for j in range(...):
            if degre(g, sommets[j]) > degre_max:
                degre_max = ...
                indice_max = j
        sommets[i], sommets[indice_max] = sommets[indice_max], sommets[i]
    return ...
\end{minted}

\question Quel est l'algorithme de tri utilisé et quelle est sa complexité ?\bareme{1}\\

\carreauxseyes{16.8}{3.2}\\

\question Citer le nom d'un tri plus efficace que celui utilisé par \mintinline{python}{tri_sommets}.\bareme{1}\\

\carreauxseyes{16.8}{1.6}\\

\question Pour écrire la fonction \mintinline{python}{welsh_powell} à partir de la fonction \mintinline{python}{coloration}, il faut prendre le code de cette dernière et changer la ligne 1 en \\
\mintinline{python}{def welsh_powell(g: dict) -> dict}.\\

Hormis cela, indiquer quelle unique ligne il faut changer et comment.\bareme{2}\\

\carreauxseyes{16.8}{4}\\

\question Indiquer la valeur renvoyée par \mintinline{python}{welsh_powell(g2)}, où \mintinline{python}{g2} représente le graphe de la question \textbf{5.} et commenter ce résultat.\bareme{2}\\

\carreauxseyes{16.8}{4}\\

\section*{Organiser un bac blanc}

Les professeurs d'un lycée ont décidé d’organiser un bac blanc pour tous les élèves de terminale. Au programme, il y aura la philosophie, les enseignements de spécialité et les enseignements optionnels Math expertes et Math complémentaires.\\

Pour la philosophie, pas de problème elle aura lieu le lundi matin, tous les élèves composent sur cette même épreuve.\\

Par contre, il faudrait que les autres épreuves soient organisées sur le moins de temps possible. Chacune prendra une demi-journée et tous les élèves suivant cet enseignement composeront au même moment avec le même sujet.\\

Vous allez devoir proposer une organisation qui utilise le moins de demi-journées possibles.

Les épreuves du bac blanc seront donc les suivantes :
\begin{multicols}{3}
\begin{itemize}
\item Spé MATH 
\item Spé PC 
\item Spé NSI
\item Spé HGGSP 
\item Spé LLCER 
\item Spé SVT
\item Spé SES
\item Opt Math Exp 
\item Opt Math Comp 
\end{itemize}
\end{multicols}
Les paires de spécialités avec éventuellement une option sont les suivantes cette année :
\begin{multicols}{3}
\begin{itemize}
\item MATH + PC
\item MATH + PC + Mexp 
\item MATH + NSI
\item MATH + NSI + Mexp 
\item MATH + SES
\item MATH + SES + Mexp
\item PC + SVT
\item PC + SVT + Mcomp 
\item PC + NSI
\item PC + NSI + Mcomp
\item NSI + SES
\item SES + HGGSP
\item SES + LLCER
\item HGGSP + LLCER
\end{itemize}
\end{multicols}
\question Que sont les sommets ? Quand y a-t-il une arête entre 2 sommets ? \bareme{1}\\

\carreauxseyes{16.8}{4}\\

\question Quel algorithme peut-on utiliser pour résoudre le problème et pourquoi ? \bareme{1}\\

\carreauxseyes{16.8}{4}\\

\question Donner le dictionnaire associé à ce graphe en le triant par degré décroissant.\bareme{1}\\

\carreauxseyes{16.8}{8}\\

\question Donner une réponse au problème.\bareme{2}\\

\carreauxseyes{16.8}{4}\\

\carreauxseyes{16.8}{24}\\


\end{document}

