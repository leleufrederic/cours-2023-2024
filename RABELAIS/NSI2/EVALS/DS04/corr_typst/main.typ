#import "ugli.typ": *

#show: doc => configDocument(title:"Corrigé du contrôle 04",doc)



= Amuse-gueule

#centeredImageWithCaption("graphe_parcours.png","Graphe à parcourir",30%)

#question() En parcourant ce graphe en profondeur à partir de U on obtient la liste suivante :
```py
['U', 'Y', 'T', 'Z', 'E', 'A', 'R', 'Q', 'P', 'O', 'I']
```
#question() En parcourant ce graphe en largeur à partir de I on obtient la liste suivante :
```py
['I', 'O', 'Q', 'Y', 'P', 'T', 'U', 'R', 'Z', 'E', 'A']
```
#questionreset()

= Implémentations des graphes non orientés

== À l'aide de la matrice d'adjacence

#question() 

#centeredImageWithCaption("graphemat.png","Graphe associé",30%)

#question() Voici la fonction #pyLine([voisins_mat]) :


#block(inset:(top:.5em,bottom:.5em,left:0%,right:0%),[
```python
def voisins_mat(lst: list, k: int) -> list:
  return [i for i in range(len(lst)) if lst[k][i]] == 1]
```])


#question() Si le graphe est d'ordre $n$ alors il faut au maximum de l'ordre de $n$ #smallcaps([opel]) pour donc la complexité de cette fonction est _linéaire_.

#question() Un graphe d'ordre 1000 est associée à une matrice d'adjacence carrée de taille 1000 qui contient donc un million d'éléments.

== À l'aide d'un dictionnaire

#question() Voici le dictionnaire :

#block(inset:(top:.5em,bottom:.5em,left:0%,right:0%),[
```python 
g1 = {"A": ["C", "D"], "B": ["D", "E"], "C": ["A", "D", "E"],
      "D": ["A", "B", "C"], "E": ["B", "C", "F"], "F": ["E"]}
```])
#question() C'est très simple 


#block(inset:(top:.5em,bottom:.5em,left:0%,right:0%),[
```python 
def voisins_dict(g: dict, s: str) -> list[str]:
    return g[s]
```])

#question() Il n'y a qu'une #smallcaps([opel]), la complexité est _constante_.

#question() Si le graphe est de taille 1000 alors le dictionnaire est également de taille 1000 (mais les valeurs peuvent être des listes d'une taille allant de 0 à 1000).

= Coloration de graphes non orientés

#question() #centeredImageWithCaption("3_graphes_soluce.png","Colorations demandées",70%)
#question() Dans le pire des cas un sommet est voisin de tous les autres et il faut n couleurs.

== Coloration séquentielle

#question() On vient de voir qu'au pire il faut n couleurs, cette ligne en constitue la liste.

#question() Voici le résultat :
- en rouge : A, B et F ;
- en vert : D et E ;
- en bleu : C.

#question() L'algorithme est glouton car à chaque étape il colorie le sommet avec la première couleur disponible dans la liste et car ce choix est définitif.

#question() #centeredImageWithCaption("g2.png","Une coloration à 2 couleurs",30%)

== Méthode de Welsh-Powell

#question() Voici la fonction :

#block(inset:(top:.5em,bottom:.5em,left:0%,right:0%),[
```py
def degre(g: dict, s: str) -> int:
    return len(g[s])
```])

#question() Voici la fonction complétée :

#block(inset:(top:.5em,bottom:.5em,left:0%,right:0%),[
```py
def tri_sommets(g: dict) -> list:
    sommets = [s for s in g.keys()]
    n = len(sommets)
    for i in range(n - 1):
        indice_max = i
        degre_max = degre(g, sommets[i])
        for j in range(i, n):
            if degre(g, sommets[j]) < degre_max:
                degre_max = degre(g, sommets[j])
                indice_max = j
        tmp = sommets[i]
        sommets[i] = sommets[indice_max]
        sommets[indice_max] = tmp
    return sommets
```])

#question() Il s'agit d'un tri par selection, sa complexité est donc _quadratique_.

#question() On a rencontré le tri fusion, qui est plus rapide, avec une complexité de l'ordre de $n log(n)$.

#question() La seule ligne à changer est la ligne 4, à remplacer par
```py
for sommet in tri_sommets(g):
```

#question() Le résultat est 

#block(inset:(top:.5em,bottom:.5em,left:0%,right:0%),[
```py
{'A': 1, 'B': 0, 'C': 0, 'D': 1}
```])

C'est ce qu'on avait établi à la question 14.

= Organiser un bac blanc

#question() Un sommet est une épreuve. 2 sommets sont reliés par une arête lorsqu'ils figurent dans une combinaison :  les 2 épreuves ne peuvent avoir lieu en même temps.

#question() L'algorithme de Welsh-Powell va nous aider : on va colorer le graphe avec un minimum de couleurs de telle sorte que 2 sommets voisins n'ont pas la même couleur.

Ces couleurs sont des demi-journées. On en choisit ainsi un minimum de sorte qu'aucune épreuve suivie par le même élève n'ait lieu au même moment.

#question() Voici le dictionnaire

```py
EDT = {
    "PC": ["Maths", "Mexp", "SVT", "Mcomp", "NSI"],
    "NSI": ["Maths", "Mexp", "PC", "Mcomp", "SES"],
    "SES": ["Maths", "Mexp", "NSI", "HGGSP", "LLCER"],
    "Maths": ["PC", "Mexp", "NSI", "SES"],
    "Mexp": ["Maths", "PC", "NSI", "SES"],
    "Mcomp": ["PC", "SVT", "NSI"],
    "SVT": ["PC", "Mcomp"],
    "HGGSP": ["SES", "LLCER"],
    "LLCER": ["SES", "HGGSP"]}
```

#question() Voici une répons au problème, obtenue en utilisant l'algorithme de Welsh-Powell :

#centeredImageWithCaption("edt.png","",50%)

Ainsi le lundi après-midi auront lieu Physique-Chimie et SES, le mardi matin NSI, HGGSP et SVT, le mardi après-midi  LLCER, Maths complémentaires et Maths expertescet le mercredi matin Maths.
