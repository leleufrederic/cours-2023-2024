/* LaTeX colors

\definecolor[named]{UGLiPurple}{HTML}{9F648C}
\definecolor[named]{UGLiRed}{HTML}{cc2936}
\definecolor[named]{UGLiOrange}{HTML}{eb8258}
\definecolor[named]{UGLiYellow}{HTML}{CFB04A}
\definecolor[named]{UGLiGreen}{HTML}{8FB349}
\definecolor[named]{UGLiDarkGreen}{HTML}{248935}
\definecolor[named]{UGLiBlue}{HTML}{267487}
\definecolor[named]{UGLiDarkBlue}{HTML}{1F466D}
*/


#let qn = counter("yo");
#let question=()=>{qn.step();[*#qn.display().* ]}
#let questionreset=()=>{qn.update(0);}
#let pyLine =(content)=>{text(font:"Source Code Pro",content)}


#let u_purple=rgb("#9F648C")
#let u_darkblue=rgb("1F466D")
#let u_blue=rgb("267487")
#let u_darkgreen=rgb("248935")
#let u_orange=rgb("eb8258")

#let genericFramedText = (f_title,f_subtitle,f_content,f_color)=>{
  v(1em)
    block(
      breakable: false,
      width: 100%,
    fill: f_color.lighten(85%),
    inset: 8pt,  
    [
      #move(dx:-.5em,dy: -1.5em)[
        #block(
        fill: f_color,
        inset: .5em,
        radius: 2pt,text(fill:white)[*#f_title  #f_subtitle *] 
      )
      ]    
      #v(-2em) #f_content #v(.5em)
  ])
}


#let defWithTitle = (title,textContent)=>{genericFramedText("Définition : ",title,textContent,u_darkgreen)}

#let defNoTitle = (textContent)=>{genericFramedText("Définition","",textContent,u_darkgreen)}

#let pyCode = (textContent)=>{genericFramedText("Python","",textContent,u_purple)}

#let cCode= (textContent)=>{genericFramedText("C","",textContent,u_blue)}

#let exoWithTitle = (title,textContent)=>{genericFramedText("Exercice","",textContent,u_orange)}

#let exoNoTitle = (textContent)=>{genericFramedText("Exercice",title,textContent,u_orange)}


#let centeredImageWithCaption = (imageFile,caption,size)=>{align(center,
  image(imageFile, width: size) + emph(caption)
)}


#let configDocument(title:none,  doc) = {
  set text(
  font: "Source Sans Pro",lang: "fr",
  size: 10pt
  )
  
  set page(
  paper: "a4",
  flipped: true,
  margin: (x: 2cm, y: 2cm),
  numbering: "1 / 1"
  )
  
  set par(
  justify: true,
  leading: 0.7em
  )

  set heading(numbering: none)
  align(center, text(size: 24pt, fill: u_blue,weight: "black", title))

  show heading.where(level:1): set text(fill: u_blue, weight:"black")
  show heading.where(level:2): set text(fill: u_orange, weight:"black")
  columns(2,gutter: 2cm, doc)
}
