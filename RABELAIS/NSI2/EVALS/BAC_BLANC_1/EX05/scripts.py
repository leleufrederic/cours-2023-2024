from random import randint


def valeur(li, saut):
    i, j = saut
    return li[j] - li[i]


def naif(lst):
    saut_max = 0
    i_max, j_max = 0, 0
    for i in range(len(lst)):
        for j in range(i + 1, len(lst)):
            if valeur(lst, (i, j)) > saut_max:
                i_max, j_max = i, j
                saut_max = lst[j] - lst[i]
    print(saut_max)
    return i_max, j_max


lst = [5, 1, 8, 3, 9, 3, 1, 4, 0]
print(lst)
print(naif(lst))
print(lst[1:2])
