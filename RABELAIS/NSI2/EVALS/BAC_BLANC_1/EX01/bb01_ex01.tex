\emph{Cet exercice porte sur les listes, les dictionnaires et la programmation de base en Python.}\\

Pour son évaluation de fin d'année, l'institut d'Enseignement Néo-moderne (EN) a
décidé d'adopter le principe du QCM. Chaque évaluation prend la forme d'une liste
de questions numérotées de 0 à 19. Pour chaque question, 5 réponses sont
proposées. Les réponses sont numérotées de 1 à 5. Exactement une réponse est
correcte par question et chaque candidat coche exactement une réponse par
question. Pour chaque évaluation, on dispose de la correction sous forme d'une liste
\mintinline{python}{corr}  contenant pour chaque question, la bonne réponse ; c'est-à-dire telle que
\mintinline{python}{corr[i]}  est la bonne réponse à la question \mintinline{python}{i}. Par exemple, on présente ci-dessous
la correction de l'épreuve 0 :
\begin{minted}{python}    
    corr0 = [4, 2, 1, 4, 3, 5, 3, 3, 2, 1, 1, 3, 3, 5, 4, 4, 5, 1, 3, 3]
\end{minted}

Cette liste indique que pour l'épreuve 0, la bonne réponse à la question 0 est 4, et
que la bonne réponse à la question 19 est 3.\\

Avant de mettre une note, on souhaite corriger les copies question par question ;
c'est-à-dire associer à chaque copie, une liste de booléens de longueur 20, indiquant
pour chaque question, si la réponse donnée est la bonne. Le candidat Tom Matt a
rendu la copie suivante pour l'épreuve 0 :

\begin{minted}{python}
copTM = [4, 1, 5, 4, 3, 3, 1, 4, 5, 3, 5, 1, 5, 5, 5, 1, 3, 3, 3, 3]        
\end{minted}



La liste de booléens correspondante est alors :
\begin{minted}{python}
        corrTM = [True, False, False, True, True, False, False, False,False, False, False, False, False, True, False, False, False, False,True, True]
\end{minted}

\begin{enumerate}
    \item	Écrire en Python une fonction \mintinline{python}{corrige}  qui prend en paramètre \mintinline{python}{cop}  et \mintinline{python}{corr}, deux listes d'entiers entre 1 et 5 et qui renvoie la liste des booléens associée à la copie \mintinline{python}{cop}  selon la correction \mintinline{python}{corr}.\\

          Par exemple, \mintinline{python}{corrige(copTM, corr0)} renvoie \mintinline{python}{corrTM}.
\end{enumerate}

La note attribuée à une copie est simplement le nombre de bonnes réponses. Si on dispose de la liste de booléens associée à une copie selon la correction, il suffit donc de compter le nombre de \mintinline{python}{True} dans la liste. Tom Matt obtient ainsi 6/20 à l'épreuve 0.\\
On remarque que la construction de cette liste de booléens n'est pas nécessaire pour calculer la note d'une copie.

\begin{enumerate}
    \setcounter{enumi}{1}
    \item	Écrire en Python une fonction \mintinline{python}{note}  qui prend en paramètre \mintinline{python}{cop}  et \mintinline{python}{corr}, deux listes d'entiers entre 1 et 5 et qui renvoie la note attribuée à la copie \mintinline{python}{cop}  selon
          la correction \mintinline{python}{corr} sans construire de liste auxiliaire.\\

          Par exemple, \mintinline{python}{note(copTM, corr0)}  renvoie 6.
\end{enumerate}
L'institut EN souhaite automatiser totalement la correction de ses copies. Pour cela, il a besoin d'une fonction pour corriger des paquets de plusieurs copies. Un paquet de copies est donné sous la forme d'un dictionnaire dont les clés sont les noms des candidats et les valeurs sont les listes représentant les copies de ces candidats. On peut considérer un paquet p1 de copies où l'on retrouve la copie de Tom Matt :

\begin{minted}[fontsize=\footnotesize]{python}
p1 = {
    ('Tom', 'Matt'): [4, 1, 5, 4, 3, 3, 1, 4, 5, 3, 5, 1, 5, 5, 5, 1, 3, 3, 3, 3],
    ('Lamb', 'Gin'): [2, 4, 2, 2, 1, 2, 4, 2, 2, 5, 1, 2, 5, 5, 3, 1, 1, 1, 4, 4],
    ('Carl', 'Roth'): [5, 4, 4, 2, 1, 4, 5, 1, 5, 2, 2, 3, 2, 3, 3, 5, 2, 2, 3, 4],
    ('Kurt', 'Jett'): [2, 5, 5, 3, 4, 1, 5, 3, 2, 3, 1, 3, 4, 1, 3, 1, 3, 2, 4, 4],
    ('Ayet', 'Finze'): [4, 3, 5, 3, 2, 1, 2, 1, 2, 4, 5, 5, 1, 4, 1, 5, 4, 2, 3, 4]
}
\end{minted}
\begin{enumerate}
    \setcounter{enumi}{2}
    \item	Écrire en Python une fonction \mintinline{python}{notes_paquet}  qui prend en paramètre un paquet de copies \mintinline{python}{p}  et une correction \mintinline{python}{corr} et qui renvoie un dictionnaire dont les clés sont les noms des candidats du paquet \mintinline{python}{p}  et les valeurs sont leurs notes selon la correction \mintinline{python}{corr}.\\

          Par exemple, \mintinline{python}{notes_paquet(p1, corr0)} renvoie
          \begin{minted}{python}
{('Tom', 'Matt'): 6,
 ('Lamb', 'Gin'): 4,
 ('Carl', 'Roth'): 2,
 ('Kurt', 'Jett'): 4,
 ('Ayet', 'Finze'): 3}       
    \end{minted}

          La fonction \mintinline{python}{notes_paquet}  peut faire appel à la fonction \mintinline{python}{note} demandée en
          question 2, même si cette fonction n'a pas été écrite.
\end{enumerate}
Pour éviter les problèmes d'identification des candidats qui porteraient les mêmes noms et prénoms, un employé de l'institut EN propose de prendre en compte les prénoms secondaires des candidats dans les clés des dictionnaires manipulés.
\begin{enumerate}
    \setcounter{enumi}{3}
    \item	Expliquer si on peut utiliser des listes de noms plutôt qu'un couple comme clés du dictionnaire.
    \item	Proposer une autre solution pour éviter les problèmes d'identification des candidats portant les mêmes prénoms et noms. Cette proposition devra prendre en compte la sensibilité des données et être argumentée succinctement.
\end{enumerate}

Un ingénieur de l'institut EN a démissionné en laissant une fonction Python énigmatique sur son poste. La directrice est convaincue qu'elle sera très utile, mais encore faut-il comprendre à quoi elle sert.\\

Voici la fonction en question :
\begin{minted}{python}
def enigme(notes):
    a = None
    b = None
    c = None
    d = {}
    for nom in notes:
        tmp = c
        if a == None or notes[nom] > a[1]:
            c = b
            b = a
            a = (nom, notes[nom])
        elif b == None or notes[nom] > b[1]:
            c = b
            b = (nom, notes[nom])
        elif c == None or notes[nom] > c[1]:
            c = (nom, notes[nom])
        else:
            d[nom] = notes[nom]
        if tmp != c and tmp != None:
            d[tmp[0]] = tmp[1]
    return (a, b, c, d)   
\end{minted}

\begin{enumerate}
    \setcounter{enumi}{5}
    \item	Calculer ce que renvoie la fonction \mintinline{python}{enigme}  pour le dictionnaire suivant :
          \begin{minted}{python}
{('Tom', 'Matt'): 6, ('Lamb', 'Gin'): 4, 
('Carl', 'Roth'): 2, ('Kurt', 'Jett'): 4, 
('Ayet', 'Finze'): 3}    
\end{minted}
    \item	En déduire ce que calcule la fonction \mintinline{python}{enigme} lorsqu'on l'applique à un dictionnaire dont les clés sont les noms des candidats et les valeurs sont leurs notes.
    \item Expliquer ce que la fonction \mintinline{python}{enigme}  renvoie s'il y a strictement moins de 3 entrées dans le dictionnaire passées en paramètre.
    \item Écrire en Python une fonction \mintinline{python}{classement}  prenant en paramètre un dictionnaire dont les clés sont les noms des candidats et les valeurs sont leurs
          notes et qui, en utilisant la fonction \mintinline{python}{enigme}, renvoie la liste des couples \mintinline{python}{((prénom, nom), note)} des candidats classés par notes décroissantes.\\

          Par exemple
          \begin{minted}{python}
classement({('Tom', 'Matt'): 6,
            ('Lamb', 'Gin'): 4,
            ('Carl', 'Roth'): 2,
            ('Kurt', 'Jett'): 4,
            ('Ayet','Finze'): 3}) 
    \end{minted}
          renvoie
          \begin{minted}{python}
[(('Tom', 'Matt'), 6),
 (('Lamb', 'Gin'), 4),
 (('Kurt', 'Jett'), 4),
 (('Ayet', 'Finze'), 3),
 (('Carl', 'Roth'), 2)]
    \end{minted}
\end{enumerate}
Le professeur Paul Tager a élaboré une évaluation particulièrement innovante de son côté. Toutes les questions dépendent des précédentes. Il est donc assuré que dès qu'un candidat s'est trompé à une question, alors toutes les réponses suivantes sont également fausses. M. Tager a malheureusement égaré ses notes, mais il a gardé
les listes de booléens associées. Grâce à la forme particulière de son évaluation, on sait que ces listes sont de la forme\\

\mintinline{python}{[True, True, ..., True, False, False, ..., False]}.\\

Pour recalculer ses notes, il a écrit les deux fonctions Python suivantes (dont la
seconde est incomplète) :

\begin{minted}{python}
def renote_express(copcorr):
    n = len(copcorr)
    c = 0
    while c < n and copcorr[c]:
        c = c + 1
    return c


def renote_express2(copcorr):
    gauche = 0
    droite = len(copcorr)
    while droite - gauche > 1:
        milieu = (gauche + droite) // 2
        if copcorr[milieu]:
            ...
        else:
            ...
    if copcorr[gauche]:
        return ...
    else:
        return ...      
\end{minted}
\begin{enumerate}
    \setcounter{enumi}{9}
    \item	Compléter le code de la fonction Python \mintinline{python}{renote_express2} pour qu'elle calcule la même chose que \mintinline{python}{renote_express}.

    \item   Déterminer les coûts en temps de \mintinline{python}{renote_express}  et \mintinline{python}{renote_express2}  en
          fonction de la longueur \mintinline{python}{n}  de la liste de booléens passée en paramètre (aucune justification n'est attendue).

    \item Expliquer comment adapter \mintinline{python}{renote_express2} pour obtenir une fonction qui corrige très rapidement une copie pour les futures évaluations de M. Tager s'il
          garde la même spécificité pour ses énoncés. Cette fonction ne devra pas construire la liste de booléens correspondant à la copie corrigée, mais directement calculer la note, on l'appelera donc en lui passant en paramètre \mintinline{python}{cop} la copie de l'élève et \mintinline{python}{corr} le corrigé.
\end{enumerate}
