\emph{Cet exercice porte sur les listes et la récursivité.}\\


Dans un tableau \textsc{Python} d'entiers \mintinline{python}{tab}, on dit que le couple d'indices \mintinline{python}{(i, j)} forme une inversion
lorsque \mintinline{python}{i < j} et \mintinline{python}{tab[i] > tab[j]}. On donne ci-dessous quelques exemples.
\begin{itemize}
    \item 	Dans le tableau \mintinline{python}{[1, 5, 3, 7]}, le couple d'indices \mintinline{python}{(1, 2)} forme une inversion car \mintinline{python}{5 > 3}.\\
          Par contre, le couple \mintinline{python}{(1, 3)} ne forme pas d'inversion car \mintinline{python}{5 < 7}.\\
          Il n'y a qu'une inversion dans ce tableau.
    \item  Il y a trois inversions dans le tableau \mintinline{python}{[1, 6, 2, 7, 3]}, à savoir les couples d'indices
          \mintinline{python}{(1, 2)}, \mintinline{python}{(1, 4)} et \mintinline{python}{(3, 4)}.
    \item On peut compter six inversions dans le tableau \mintinline{python}{[7, 6, 5, 3]} : les couples d'indices
          \mintinline{python}{(0, 1)}, \mintinline{python}{(0, 2)}, \mintinline{python}{(0, 3)}, \mintinline{python}{(1, 2)}, \mintinline{python}{(1, 3)} et \mintinline{python}{(2, 3)}.
\end{itemize}
On se propose dans cet exercice de déterminer le nombre d'inversions dans un tableau
quelconque.

\subsection*{Questions préliminaires}

\begin{enumerate}
    \item 	Expliquer pourquoi le couple \mintinline{python}{(1, 3)} est une inversion dans le tableau \mintinline{python}{[4, 8, 3, 7]}.
    \item 	Justifier que le couple \mintinline{python}{(2, 3)} n'en est pas une.
\end{enumerate}
\subsection*{Partie A : Méthode itérative}

Le but de cette partie est d'écrire une fonction itérative \mintinline{python}{nombre_inversions} qui renvoie le
nombre d'inversions dans un tableau.\\
Pour cela, on commence par écrire une fonction \mintinline{python}{fonction1} qui sera ensuite utilisée pour écrire la fonction \mintinline{python}{nombre_inversions}.
\begin{enumerate}
    \item 	On donne la fonction suivante.

          \begin{pyc}
              \begin{minted}{python}
        def fonction1(tab, i):
            nb_elem = len(tab)
            cpt = 0
            for j in range(i+1, nb_elem):
                if tab[j] < tab[i]:
                cpt += 1
            return cpt
    \end{minted}
          \end{pyc}

          \begin{enumalph}
              \item 	 Indiquer ce que renvoie \mintinline{python}{fonction1(tab, i)} dans les cas suivants.
              \begin{itemize}
                  \item 	  cas n°1 : \mintinline{python}{tab = [1, 5, 3, 7]} et \mintinline{python}{i = 0}.
                  \item 	 cas n°2 : \mintinline{python}{tab = [1, 5, 3, 7]} et \mintinline{python}{i = 1}.
                  \item    cas n°3 : \mintinline{python}{tab = [1, 5, 2, 6, 4]} et \mintinline{python}{i = 1}.
              \end{itemize}
              \item Expliquer ce que permet de déterminer cette fonction.
          \end{enumalph}
    \item  En utilisant la fonction précédente, écrire une fonction \mintinline{python}{nombre_inversions(tab)} qui
          prend en argument un tableau et renvoie le nombre d'inversions dans ce tableau.\\
          On donne ci-dessous les résultats attendus pour certains appels.
          \begin{minted}{pycon}
>>> nombre_inversions([1, 5, 7])
0

>>> nombre_inversions([1, 6, 2, 7, 3])
3

>>> nombre_inversions([7, 6, 5, 3])
6
\end{minted}

    \item  Quel est l'ordre de grandeur de la complexité en temps de l'algorithme obtenu ?\\
          Aucune justification n'est attendue.
\end{enumerate}
\subsection*{Partie B : Méthode récursive}

Le but de cette partie est de concevoir une version récursive de la fonction de la partie précédente.
On définit pour cela des fonctions auxiliaires.

\begin{enumerate}
    \item 	Donner le nom d'un algorithme de tri ayant une complexité meilleure que quadratique.\\
\end{enumerate}
Dans la suite de cet exercice, on suppose qu'on dispose d'une fonction \mintinline{python}{tri(tab)} qui prend en
argument un tableau et renvoie un tableau contenant les mêmes éléments rangés dans l'ordre
croissant.
\begin{enumerate}\setcounter{enumi}{1}
    \item Écrire une fonction \mintinline{python}{moitie_gauche(tab)} qui prend en argument un tableau \mintinline{python}{tab} et
          renvoie un nouveau tableau contenant la moitié gauche de \mintinline{python}{tab}. Si le nombre d'éléments
          de \mintinline{python}{tab} est impair, l'élément du centre se trouve dans cette partie gauche.
          On donne ci-dessous les résultats attendus pour certains appels.
          \begin{minted}{pycon}
>>> moitie_gauche([])
[]

>>> moitie_gauche([4, 8, 3])
[4, 8]

>>> moitie_gauche ([4, 8, 3, 7])
[4, 8]
\end{minted}
\end{enumerate}
Dans la suite, on suppose qu'on dispose de la fonction \mintinline{python}{moitie_droite(tab)} qui renvoie la
moitié droite sans l'élément du milieu
\begin{enumerate}\setcounter{enumi}{2}
    \item On suppose qu'une fonction \mintinline{python}{nb_inv_tab(tab1, tab2)} a été écrite.\\
          Cette fonction renvoie le nombre d'inversions du tableau obtenu en mettant bout à bout les tableaux
          \mintinline{python}{tab1} et \mintinline{python}{tab2}, à condition que \mintinline{python}{tab1} et \mintinline{python}{tab2} soient triés dans l'ordre croissant.\\
          On donne ci-dessous deux exemples d'appel de cette fonction :
          \begin{minted}{python}
>>> nb_inv_tab([3, 7, 9], [2, 10])
3
>>> nb_inv_tab([7, 9, 13], [7, 10, 14])
3
\end{minted}

          En utilisant la fonction \mintinline{python}{nb_inv_tab} et les questions précédentes, écrire une fonction
          récursive \mintinline{python}{nb_inversions_rec(tab)} qui permet de calculer le nombre d'inversions
          dans un tableau.\\
          Cette fonction renverra le même nombre que \mintinline{python}{nombre_inversions(tab)} de la partie A.\\
          On procédera de la façon suivante :
          \begin{itemize}
              \item 	Séparer le tableau en deux tableaux de tailles égales (à une unité près).
              \item 	Appeler récursivement la fonction \mintinline{python}{nb_inversions_rec} pour compter le nombre
                    d'inversions dans chacun des deux tableaux.
              \item   Trier les deux tableaux (on rappelle qu'une fonction de tri est déjà définie).
              \item   Ajouter au nombre d'inversions précédemment comptées le nombre renvoyé par la
                    fonction \mintinline{python}{nb_inv_tab} avec pour arguments les deux tableaux triés.
          \end{itemize}
\end{enumerate}
