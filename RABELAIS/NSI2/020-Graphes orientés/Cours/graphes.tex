\documentclass[10pt,firamath,cours]{nsi}
\begin{document}
\setcounter{chapter}{15}
\chapter{Graphes}
\section{Introduction}
\subsection{Plusieurs représentations}

Abel, Brieuc, Corentin, David et Ewen postent des messages sur un réseau social. Un message peut-être  «  aimé »  par n'importe quel utilisateur, y compris son créateur.\\
On regarde, sur une période de deux semaines, qui a aimé les messages de qui. Voici les résultats :
\begin{itemize}
    \item 	Abel a aimé des messages de Corentin  et David;
    \item 	Brieuc a aimé ses messages et ceux de Corentin;
    \item 	Corentin a aimé les messages de David;
    \item 	David a aimé ses propres messages;
    \item 	Ewen a aimé les messages d'Abel.
\end{itemize}
\begin{center}
    \includegraphics[width=7cm]{img/graphe1.png}
\end{center}
Ces résultats permettent de produire un \textit{graphe orienté} :
\begin{itemize}
    \item 	les \textit{sommets du graphe} représentent les personnes;
    \item 	les \textit{arêtes} sont des flèches qui représentent le fait que la personne de départ aime les messages de celle d'arrivée.
\end{itemize}
On peut aussi représenter les résultats dans un tableau :
\begin{center}
    \tabstyle[UGLiBlue]
    \begin{tabular}{c|c|c|c|c|c}
        \hline\cellcolor{white}
                                                          & \cellcolor{UGLiOrange}\ccell A & \cellcolor{UGLiOrange}\ccell B & \cellcolor{UGLiOrange}\ccell C & \cellcolor{UGLiOrange}\ccell D & \cellcolor{UGLiOrange}\ccell E \\
        \hline
        \cellcolor{UGLiOrange}\ccell aime les messages de & C,D                            & B,C                            & D                              & D                              & A                              \\
        \hline
    \end{tabular}
\end{center}

On peut aussi recopier le tableau en donnant pour chaque personne la liste de ses  «  followers »  (personnes qui ont aimé ses messages) :

\begin{center}
    \tabstyle[UGLiBlue]
    \begin{tabular}{c|c|c|c|c|c}
        \hline\cellcolor{white}
                                               & \cellcolor{UGLiOrange}\ccell A & \cellcolor{UGLiOrange}\ccell B & \cellcolor{UGLiOrange}\ccell C & \cellcolor{UGLiOrange}\ccell D & \cellcolor{UGLiOrange}\ccell E \\
        \hline
        \cellcolor{UGLiOrange}\ccell followers & B,E                            & B                              & A,B                            & A,C,D                          & ---                            \\
        \hline
    \end{tabular}
\end{center}
On peut aussi présenter les données ainsi :
\begin{center}
    \includegraphics[width=7cm]{img/prematrice.png}
\end{center}
Il y a donc plusieurs manières de représenter un graphe orienté.

\subsection{Organiser un graphe orienté}

On considère des individus qui ont infectés par une maladie et on place une flèche pour signifier que tel individu a contaminé tel autre individu.\\
On obtient ce résultat :
\begin{center}
    \includegraphics[width=7cm]{img/graphe2_random.png}
\end{center}
C'est le fouillis, n'est-ce pas ? \\
En déplaçant les sommets du graphe on peut le présenter ainsi :
\begin{center}
    \includegraphics[width=7cm]{img/graphe2_radial.png}
\end{center}
C'est le même graphe mais on y voit plus clair... et on y voit encore plus clair comme ça :
\begin{center}
    \includegraphics[width=7cm]{img/graphe2_tree.png}
\end{center}
On voit clairement le  «  patient zéro » . On peut aussi rajouter des flèches quand la contamination n'est pas directe mais s'est faite  «  par une ou plusieurs personnes interposées » . On obtient ceci
\begin{center}
    \includegraphics[width=7cm]{img/graphe2_transitif.png}
\end{center}
Le but de ce chapitre est de formaliser tout cela (et plus).

\section{Représentations d'un graphe orienté}
\subsection{Premières notions}
\begin{definition}[ : graphe, sommet, arc]
    Un graphe orienté, c'est la donnée de
    \begin{itemize}
        \item 	un ensemble $S=\lbrace s_1;\,...;\,s_n\rbrace$ de \textit{sommets};
        \item 	un ensemble $A$ composé d'\textit{arcs} du type $(s_i;\,s_j)$, qui indiquent qu'il y a  «  une flèche »  partant du sommet $s_i$ et allant au sommet $s_j$.:\\ $s_i$ est appelé l'\textit{origine} de l'arc et $s_j$ son \textit{extrémité}.
    \end{itemize}
\end{definition}

\begin{exemple}[]
    Ici l'ensemble des sommets est $\lbrace A;\,B;\,C;\,D\rbrace$.\\
    L'ensemble des arcs est :\\
    $\lbrace (A;B);\,(B;A);\,(B;C);\,(B;D);\,(C;D)\rbrace$.
    \begin{center}
        \includegraphics[width=5cm]{img/ex_graphe_oriente.png}
    \end{center}
\end{exemple}

\begin{exercice}[]
    Donner l'ensemble des sommets et l'ensemble des arêtes du graphe suivant.
    \begin{center}
        \includegraphics[width=7cm]{img/exo_graphe.png}
    \end{center}
\end{exercice}

\begin{definition}[ : boucle]
    Un arc dont l'origine et l'extrémité sont confondues d'appelle une \textit{boucle}.
\end{definition}

\subsection{Successeurs, prédécesseurs}

\begin{definition}[ : successeur, prédécesseur]
    Soit un graphe de sommets $S$ et d'arcs $A$. Soit $s$ un sommet.\\
    On note $\Gamma^+(s)$ l'ensemble des \textit{successeurs de $s$}.\\
    C'est l'ensemble des extrémités des arcs \textit{partant de} $s$.\\
    De même, on note $\Gamma^-(s)$ l'ensemble des \textit{prédécesseurs de $s$}.\\
    C'est l'ensemble des origines des arcs \textit{arrivant sur} $s$.\\
\end{definition}

\begin{exemple}[s]
    Dans ce graphe, il y a 4 arcs d'origine D. Leurs extrémités sont les points C,D,E et F.\\ Ainsi $\Gamma^+(D)=\lbrace C;\,D;\,E;\,F\rbrace$.
    \begin{center}
        \includegraphics[width=7cm]{img/successeurs.png}
    \end{center}
    On peut retrouver le graphe à partir du tableau des successeurs.
    \begin{center}
        \tabstyle[UGLiGreen]
        \begin{tabular}{|c|c|c|c|c|c|c|}
            \hline
            \ccell sommet      & \ccell A & \ccell B & \ccell C & \ccell D   & \ccell E & \ccell F \\
            \hline
            \ccell successeurs & B        & A, C, E  & ---      & C, D, E, F & B        & C        \\
            \hline
        \end{tabular}
        \includegraphics[width=7cm]{img/predecesseurs.png}
    \end{center}
    Dans ce graphe, il y a 3 arcs d'extrémité C.\\
    Leurs origines sont les points B,D et F.\\ Ainsi $\Gamma^-(C)=\lbrace B;\,D;\,F\rbrace$.
    On peut retrouver le graphe à partir du tableau des prédécesseurs.
    \begin{center}
        \tabstyle[UGLiGreen]
        \begin{tabular}{|c|c|c|c|c|c|c|}
            \hline
            \ccell sommet        & \ccell A & \ccell B & \ccell C & \ccell D & \ccell E & \ccell F \\
            \hline
            \ccell prédécesseurs & B        & A, E     & B, D, F  & B, D     & B, D     & D        \\
            \hline
        \end{tabular}
    \end{center}
\end{exemple}

\begin{exercice}[]
    Pour chacun des graphes, donne le tableau des successeurs et celui des prédécesseurs (attention : pour le graphe de gauche, on a dessiné des arcs bidirectionnels, chacun compte pour 2 arcs).
    \begin{center}
        \includegraphics[width=7cm]{img/exo1_graphe1.png}\hspace*{2em}\includegraphics[width=6cm]{img/exo1_graphe2.png}
    \end{center}
\end{exercice}

\begin{exercice}[]
    Dessine le graphe correspondant au tableau de successeurs.
    \begin{center}
        \begin{tabular}{|c|c|c|c|c|}
            \hline
            \ccell sommet                            & \ccell A & \ccell B & \ccell C & \ccell D \\
            \hline
            \cellcolor{UGLiOrange}\ccell successeurs & A,B,C    & B,C,D    & C,D,A    & D,A,B    \\
            \hline
        \end{tabular}
    \end{center}
\end{exercice}

\begin{exercice}[]
    Dessine le graphe correspondant au tableau de prédécesseurs.
    \begin{center}
        \begin{tabular}{|c|c|c|c|c|c|}
            \hline
            \ccell sommet                              & \ccell A & \ccell B & \ccell C & \ccell D \\
            \hline
            \cellcolor{UGLiOrange}\ccell prédécesseurs & A,D      & ---      & A,B,D    & A        \\
            \hline
        \end{tabular}
    \end{center}
\end{exercice}

\subsection{Matrice d'adjacence}

\begin{definition}[ : matrice d'adjacence]
    Soit un graphe G de sommets  $S=\lbrace s_1;\,...;\,s_n\rbrace$ et d'arcs A. On appelle \textit{matrice d'adjacence de} G la matrice carrée d'ordre n $M=(m_{ij})$ telle que
    \begin{itemize}
        \item 	$m_{ij}=1$ s'il y a un arc partant de $s_i$ et arrivant sur $s_j$;
        \item 	$m_{ij}=0$ sinon.
    \end{itemize}
\end{definition}

\begin{exemple}[]
    En écrivant les sommets dans l'ordre alphabétique, la matrice d'adjacence du graphe ci-dessous est
    $$M=\begin{matrice}
            1 & 1         & 0 & 0              & 0 \\
            0 & 0         & 1 & 0              & 0 \\
            0 & \boxed{1} & 0 & 0              & 1 \\
            1 & 0         & 1 & 0              & 1 \\
            0 & 1         & 0 & {\color{red}1} & 0
        \end{matrice}$$
    \begin{center}
        \includegraphics[width=6cm]{img/matr_adj1.png}
    \end{center}
    L'élément encadré de M est à la 3\eme ligne et à la 2\eme colonne : c'est $m32$. Il vaut 1 et signifie qu'il y a un arc partant du sommet 3, donc C, et allant au sommet 2, donc B.\\
    De même, du 5\eme sommet E, il existe un arc allant vers le 4\eme (D), donc $m_{54}=1$ (en rouge).
\end{exemple}

\begin{remarque}[]
    Dans une matrice d'adjacence
    \begin{itemize}
        \item 	Les lignes correspondent au points de départ, les colonnes au point d'arrivée;
        \item 	Sur une ligne donnée(la i\eme par exemple), on peut lire \textit{tous les successeurs} de $s_i$;
        \item 	Sur une colonne donnée, (la j\eme par exemple), on lite \textit{tous les prédécesseurs} de $s_j$;
    \end{itemize}
\end{remarque}

\begin{exercice}[]
    On considère un graphe dont l'ensemble des sommets est $\lbrace A;\,B;\,C;\,D;\,E;\,F\rbrace$, et dont la matrice d'adjacence est
    $$ M=\begin{matrice}
            1 & 1 & 0 & 1 & 0 & 1 \\
            0 & 0 & 0 & 1 & 0 & 1 \\
            1 & 0 & 0 & 0 & 1 & 1 \\
            1 & 0 & 1 & 1 & 1 & 0 \\
            0 & 1 & 0 & 1 & 1 & 1 \\
            1 & 0 & 1 & 0 & 1 & 0
        \end{matrice}$$
    
    \begin{enumerate}
        \item 	Donner le tableau des successeurs de chaque sommet.
        \item 	Donner le tableau des prédécesseurs de chaque sommet.
        \item 	Représenter ce graphe.
    \end{enumerate}
\end{exercice}

\begin{exercice}[]
    Soit $n\in\N$, combien y a-t-il de graphes orientés à $n$ sommets ?
\end{exercice}

\section{Chemins et circuits}

\begin{definition}[s : chemin, longueur]
    
    On considère un graphe orienté.\\
    Un \textit{chemin} est une succession de sommets dans un ordre donné, chacun étant relié au suivant par un arc.\\
    La \textit{longueur} du chemin, c'est le nombre d'arcs qui composent le chemin. C'est aussi le nombre de sommets qui composent le chemin mois un.
\end{definition}

\begin{exemple}[]
    $(F,\,C,\,D,\,E)$ est un chemin de longueur 3.\\
    $(F,\,C,\,B,\,E)$ n'en est pas un car l'arc $(C,\,B)$ n'existe pas.
    \begin{center}
        \includegraphics[width=7cm]{img/chemin.png}
    \end{center}
\end{exemple}

\begin{definition}[s : chemin hamiltonien,circuit]
    
    Un \textit{chemin hamiltonien} est un chemin qui passe une et une seule fois par \textit{chaque} sommet.\\
    Un \textit{circuit} est un chemin dont le premier et le dernier sommet sont identiques (un chemin  «  fermé »  en quelque sorte).
\end{definition}


\begin{exemple}[s]
    
    \begin{center}
        \includegraphics[width=7cm]{img/circuit.png}
    \end{center}
    $(C,\,D,\,E,\,B,\,C)$ est un circuit de longueur 4.
    
    \begin{center}
        \includegraphics[width=7cm]{img/chemin_hamiltonien.png}
    \end{center}
    $(F,\,C,\,D,\,E,\,B,\,A)$ est un chemin hamiltonien.
    
\end{exemple}

\begin{remarque}[]
    Un graphe orienté \textit{ne possède pas toujours} de circuit hamiltonien (à gauche) ou bien peut en posséder plusieurs (à droite).
    \begin{center}
        \includegraphics[width=4cm]{img/pas_de_ch.png}\hspace{2em}\includegraphics[width=4cm]{img/plusieurs_ch.png}
    \end{center}
\end{remarque}

\begin{exercice}[]
    Trouve un chemin hamiltonien, un circuit de longueur 3 et un autre de longueur 4.
    \begin{center}
        \includegraphics[width=7cm]{img/ex_circuit_ch.png}
    \end{center}
\end{exercice}

\section{Implémentations des graphes orientés en Python}

\subsection{À l'aide de la matrice d'adjacence}

\begin{center}
    \includegraphics[width=4cm]{img/graphe_matrice}
\end{center}
\begin{pyc}
    \begin{minted}{python}
G = [[0, 1, 1, 0], # successeurs de A
     [0, 0, 1, 1], # successeurs de B
     [1, 1, 0, 0], # successeurs de C
     [1, 0, 1, 0]] # successeurs de D
        
    \end{minted}
\end{pyc}

Cette implémentation est très simple à mettre en \oe uvre et les fonctions \mintinline{python}{successeurs} et \mintinline{python}{arc} sont faciles à coder.\\

Mais on ne peut pas facilement ajouter d'autres sommets et pour $n$ sommets on a une matrice $n\times n$, ce qui prend beaucoup de place surtout s'il n'y a pas beaucoup d'arcs. Enfin on n'a pas les noms des sommets dans la matrice, c'est à nous de connaître la correspondance.
\begin{remarque}[s]
    \begin{itemize}
        \item 	On peut très facilement encapsuler la matrice et les fonctions dans une classe.
        \item 	Attention, en \textsc{Python}, les indices commencent à 0, donc il y a un petit « décalage» par rapport à la matrice théorique.
    \end{itemize}
\end{remarque}


\subsection{À l'aide d'un dictionnaire de successeurs}
    \begin{center}
        \includegraphics[width=4cm]{img/graphe_matrice}
    \end{center}

\begin{pyc}
    \begin{minted}{python}
G = {'A' : ['B','C'], # les sommets sont les clés
     'B' : ['C','D'], # et les valeurs les listes 
     'C' : ['A','B'], # des successeurs de chaque
     'D' : ['A','C']} # clé
    \end{minted}
\end{pyc}

C'est intéressant car on peut facilement ajouter d'autres sommets, on dispose de leurs noms, et c'est une solution assez « élégante ».\\
Ceci dit, si le nombre de sommets (et surtout d'arcs) est grand, cette implémentation prendra beaucoup plus de place en mémoire qu'une simple matrice d'adjacence.

\begin{remarque}[]
    Encore une fois, il est judicieux d'encapsuler ce dictionnaire dans une classe.\\ 
    On pourra alors définir des méthodes telles que :
    \begin{itemize}
        \item 	\texttt{liste\_sommets}
        \item 	\texttt{successeurs}
        \item 	\texttt{predecesseurs}
        \item 	\textit{Et c\ae tera}
    \end{itemize}
    
\end{remarque}

\end{document}