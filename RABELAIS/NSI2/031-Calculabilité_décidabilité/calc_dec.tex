\documentclass[10pt,firamath,cours]{nsi}
\begin{document}
\setcounter{chapter}{26}
\chapter{Calculabilité et décidabilité}

\section{Un peu d'Histoire}


Depuis l'Antiquité l'humanité utilise des \emph{méthodes de calcul} pour résoudre des problèmes, mais jusqu'au début du vingtième siècle on n'avait aucune définition précise de ce qu'est une méthode de calcul.\\


Pourtant lors du Congrès International des Mathématiciens de Paris de 1900, David Hilbert a formulé 24 problèmes, dont le dixième dont l'énoncé peut être vulgarisé en :\\

\emph{« Existe-t-il une méthode générale pour déterminer quelles équations diophantiennes ont des solutions et quelles
    équations diophantiennes n'en ont pas ? »}


\begin{definition}[ : équation diophantienne]
    C'est une équation
    \begin{itemize}
        \item	à une ou plusieurs inconnues ;
        \item	polynomiale : ne figurent que des puissances entières des inconnues ;
        \item à coefficients entiers ;
        \item dont on cherche des solutions entières.
    \end{itemize}
\end{definition}

\begin{exemple}[s]
    $ x^2-3x+2 = 0 $ est une équation diophantienne de solutions 1 et 2.\\

    $ x^2+y^2=z^2 $ en est une autre, ses solutions sont les triplets pythagoriciens : $(3,4,5)$, $(5,12,13)$ et beaucoup d'autres...\\

    $ 3x^5+2x y z^3+y^2-z+2=0 $ en est une dernière...

\end{exemple}

Ce sont les mathématiciens Church, Kleene, Turing et Gödel qui, entre 1931 et 1936 ont identifié la notion d'algorithme en tant que méthode de calcul.\\
Chacun d'entre eux s'intéressa à sa manière et plus ou moins indépendamment des autres à une catégorie de fonctions. Ils reconnurent ensuite que ces différentes catégories coïncidaient et les appelèrent \emph{la classe des fonctions calculables}.\\
C'est ce qu'on appelle la \emph{thèse de Church} : tous les modèles de calculs qui ont été utilisés pour expliquer ce qu'est une fonction calculable sont équivalents.

\section{Calculabilité, décidabilité}

\subsection{Fonction calculable}

\begin{definition}[ : fonction calculable]
    Une fonction est dite calculable s'il existe une façon finie de la décrire qui permette effectivement d'en calculer toutes les valeurs.
\end{definition}


Cette définition fixe également la notion d'algorithme. Ce sont les travaux de Turing qui ont abouti la définition
d'algorithme la plus pratique à utiliser : en 1936 la notion d'algorithme était acquise.

\begin{definition}[ : algorithme]
    Un algorithme est suite finie d'instructions qu'une machine de Turing peut effectuer.
\end{definition}


Depuis, on s'est employé à créer des entités distinctes des machines de Turing, mais qui sont dites \emph{Turing-complètes}, c'est-à-dire qui peuvent « faire tout ce que peut faire une machine de Turing ».\\

Les langages de programmation que nous connaissons (Python, Java, C++ \emph{et cætera}) sont Turing-complets (ouf !), mais HTML seul (sans CSS) n'est pas Turing-complet.\\


Dans le cadre de notre cours on peut donc reformuler :

\begin{definition}[fonction calculable (bis)]
    Une fonction est calculable si on peut la programmer (en Python par exemple).
\end{definition}

\subsection{Problème décidable}

Un problème de \emph{décision} peut s'envisager ainsi :

\begin{center}
    \tabstyle[UGLiOrange]
    \begin{tabular}{p{5cm}|p{5cm}|p{5cm}}
        \ccell Cas général                                                   & \ccell Exemple 1                 & \ccell Exemple 2                        \\
        Nom du problème                                                      & Primalité d'un entier            & Problème de l'arrêt                     \\
        Donnée : spécification d'une instance du problème                    & un entier naturel                & programme (écrit en Python par exemple) \\
        Réponse : propriété qui spécifie les instances positives du problème & décider si ce nombre est premier & décider si ce programme  s'arrête       \\
    \end{tabular}
\end{center}

Sans rentrer dans les détails, on peut associer aux problèmes de décision des fonctions de décision. Un problème est
alors appelé \emph{décidable} si la fonction associée est calculable. Cela nous permet de formuler une définition :

\begin{definition}[ : Problème décidable]
    Un problème est décidable si on peut écrire une fonction (en Python ou autre) qui
    \begin{itemize}
        \item	en entrée prend les données du problème ;
        \item	renvoie \mintinline{python}{True}  ou \mintinline{python}{False}  suivant que les données satisfont le problème ou non.
    \end{itemize}
\end{definition}
Par exemple, le problème « $n\in\N$ est-il premier ? » est décidable :

\begin{pyc}
    \begin{minted}{python}
        def est_premier(n: int) -> bool:
            for i in range(2, n):
                if n % i == 0:
                    return False
            return True
    \end{minted}
\end{pyc}

L'algorithme implémenté en Python est fort peu efficace, mais là n'est pas le problème : il existe. Donc le problème de la primalité d'un entier positif est décidable.

Dire qu'un problème est \emph{indécidable} signifie donc qu'il n'existe pas d'algorithme permettant de résoudre ce problème dans le cas général (quelles que soient les données du problème).

\subsection{Nombre calculable}

\begin{definition}[ : nombre calculable]
    Un nombre réel est \emph{calculable} si et seulement si il existe un algorithme permettant de calculer autant de décimales que l'on veut de l'écriture en base 10 de ce réel.
\end{definition}

Évidemment si le nombre n'est pas une fraction, son écriture décimale n'est pas périodique, il est donc hors de question de chercher à les connaître toutes (sauf cas particulier où l'on a nous même fabriqué l'écriture décimale du nombre).

Par exemple, on peut montrer que le nombre $e$ (celui qui permet de construire la fonction exponentielle) vaut
$$\sum_{k=0}^{+\infty}\frac{1}{k!}=1+\frac{1}{1!}+\frac{1}{2!} +\frac{1}{3!}+\ldots$$

Avec $0!=1$, rappelons-le.

Il y a une infinité de termes dans la somme (c'est une limite), mais en prenant un nombre suffisant des premiers termes de cette somme, on peut en déterminer autant de décimales de $e$ que l'on désire.

\begin{exercice}[]
    \begin{enumerate}
        \item	Programmer une fonction \mintinline{python}{factorielle}.
        \item	Programmer une fonction \mintinline{python}{approx} qui
              \begin{itemize}
                  \item	en entrée prend un entier positif $n$ ;
                  \item	renvoie $\sum_{k=0}^{n}\frac{1}{k!}$ en calculant suivant la règle de la photo de classe pour minimiser les erreurs d'arithmétique en virgule flottante.
              \end{itemize}


        \item Programmer la fonction \mintinline{python}{decimales_de_e}  qui
              \begin{itemize}
                  \item	en entrée prend un entier $d$ positif ;
                  \item	renvoie l'écriture décimale de $e$ avec les $d$ premières décimales exactes.
              \end{itemize}
    \end{enumerate}
    Pour la dernière question, on pourra utiliser la fonction \mintinline{python}{round} :  \mintinline{python}{round(a,4)} tronque \mintinline{python}{a}  à la quatrième décimale.

\end{exercice}

\begin{remarque}[]
    écrit tel quel, ce programme ne permet pas d'afficher beaucoup de décimales : 17 au maximum. Cela est dû au format de représentation des \mintinline{python}{float}  de Python. Pour en avoir plus, il faudrait s'y prendre autrement\ldots

\end{remarque}


\section{Un programme est une donnée comme les autres}

\subsection{C'est déjà vrai pour les fonctions}

Lorsque nous avons étudié la programmation fonctionnelle, nous avons expliqué qu'il est possible de passer une fonction en paramètre à une autre fonction.

Voici un exemple :
\begin{pyc}
    \begin{minted}{python}
  def nulle_en_zero(f):
      epsilon = 10 ** - 6
      return abs(f(0)) < epsilon      
    \end{minted}
\end{pyc}

La fonction \mintinline{python}{nulle_en_zero} :
\begin{itemize}
    \item	prend en entrée une fonction $f$ qui est censée être une fonction numérique définie « au voisinage de zéro » ;
    \item	renvoie \mintinline{python}{True} si $f(0)$ est très petit et \mintinline{python}{False}  sinon.
\end{itemize}

\subsection{Cela reste vrai pour les programmes}

De la même manière, on peut donner un programme en entrée d'un autre programme. D'ailleurs, on le fait plus souvent
qu'on ne le croit :

\begin{itemize}
    \item	Sous Linux / macOS : \mintinline{bash}{shell-unix-generic cat fichier.txt} demande au programme \mintinline{bash}{shell-unix-generic cat} d'afficher le contenu du fichier \mintinline{bash}{fichier.txt}
    \item	Sous Windows / Linux / macOS : \mintinline{bash}{python fichier.py}
          donne le fichier \texttt{fichier.py} à l'interpréteur Python pour que celui-ci l'exécute.
    \item Lorsqu'on a écrit un programme en langage C, tel celui-ci :
          \begin{minted}{c}
    #include <stdio.h>

    int main() {
        printf("Hello World!");
        return 0;
    }
    \end{minted}
          et qu'on a sauvegardé ce fichier texte sous le nom \texttt{hello\_world.c}, alors on utilise un compilateur C qui prend en entrée ce fichier et produit (sous Windows) un fichier exécutable \texttt{hello\_world.exe}.
    \item  Quand on compresse un programme, on utilise un autre programme pour le compresser.

    \item  Quand on utilise un antivirus, il prend (entre autres) des programmes et les analyse.

    \item \emph{Et cætera}.

\end{itemize}



\section{Des problèmes indécidables}

\subsection{Le problème de l'arrêt}

C'est Alan Turing qui a prouvé le résultat suivant. Il utilisait (évidemment) des machines de Turing dans sa preuve.

Puisque Python est Turing-complet, nous utiliserons Python !

Lorsqu'on programme une fonction, il se peut qu'elle ne s'arrête pas. C'est souvent très embêtant, d'où la question
suivante :

\emph{Existe-t-il une fonction permettant de déterminer si une fonction donnée s'arrête où non et ce, quelle que soit cette
    fonction donnée et les paramètres qu'on lui passe ?}

Cette question pose ce que l'on appelle \emph{le problème de l'arrêt}.

Supposons qu'une telle fonction existe, appelons-là \mintinline{python}{A}  :
\begin{pyc}
    \begin{minted}{python}
        def A(f,x) -> bool :
            """
            Renvoie True si f(x) s'arrête, False sinon
            """
        
    \end{minted}
\end{pyc}

Construisons alors la fonction \mintinline{python}{D}  suivante :

\begin{pyc}
    \begin{minted}{python}
        def D(f):
            if A(f, f) is True:
                while True:
                    pass
            else:
                return True    
    \end{minted}
\end{pyc}

Que peut-on dire de \mintinline{python}{D(D)}  ?

\begin{itemize}
    \item	Si \mintinline{python}{python D(D)}  se termine, alors cela signifie que \mintinline{python}{python A(D, D)} est faux, donc que \mintinline{python}{python D(D)} ne se termine pas.
    \item	Si \mintinline{python}{python D(D)} ne se termine pas, alors on est entré dans la boucle \mintinline{python}{while}, donc \mintinline{python}{python A(D, D)} est vrai et alors \mintinline{python}{python D(D)} se termine.
\end{itemize}
Dans les deux cas on arrive à une contradiction. Ainsi notre supposition est erronée : la fonction \mintinline{python}{A} n'existe pas. Autrement dit :

\emph{il n'existe pas de programme permettant de déterminer systématiquement
    si un programme donné s'arrête ou non. Le problème de l'arrêt est indécidable.}

\subsection{D'autres exemples}

Le problème précédent est assez abstrait, il existe des problèmes plus concrets qui sont pourtant indécidables, tel le problème de correspondance de Post ou le problème de pavage du plan avec un nombre fini de forme polygonales données.\\

Le dixième problème de Hilbert est indécidable : on a montré en 1970 qu'un algorithme général permettant de déterminer si une équation diophantienne a des solutions ou non n'existe pas.

\section{Des nombres incalculables ?}

\picright{0.2}{img/chaitin}{
Il est facile de montrer que la majorité des nombres réels sont incalculables, mais pour autant, ce n'est qu' « assez récemment » que Gregory Chaitin a exhibé un nombre $Omega$ non calculable.\\

Sa définition est complexe et repose sur les probabilités d'arrêt de certains algorithmes.}

\section{Peut-on enfoncer le clou encore un peu plus ?}
\picleft{0.3}{img/Godel}{

    Oui ! En 1931 Kurt Gödel a démontré le \emph{premier théorème d'incomplétude }, riche en implications tant scientifiques que
    philosophiques.


    Pour démontrer des théorèmes, les mathématiciens doivent utiliser un \emph{système formel} avec des axiomes, \emph{et cætera}.
    Son théorème énonce que tout système formel assez puissant pour démontrer les théorèmes d'arithmétiques (donc en particulier celui que nous utilisons sans vraiment y faire attention) est \emph{incomplet} : il existe des propositions
    pour lesquelles il n'existe ni de preuve qu'elles sont vraies, ni de preuve qu'elles sont fausses.
    
    Ce résultat est à mettre en parallèle avec les notions précédentes même s'il n'est pas de même nature : il dépend du système formel dans lequel on se place, au contraire de la décidabilité et la calculabilité...}



\end{document}