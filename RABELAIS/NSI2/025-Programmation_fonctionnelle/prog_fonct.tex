\documentclass[10pt,firamath,cours]{nsi}
\begin{document}

\setcounter{chapter}{20}
\chapter{Programmation fonctionnelle}
\introduction{Une fonction est une donnée comme les autres.}
\begin{encadrecolore}{À retenir}{UGLiGreen}
    Tout comme la programmation récursive ou orientée objet, la \textit{programmation fonctionnelle} est un des multiples paradigmes de programmation. \textsc{Python}, langage multi-paradigme, autorise la programmation fonctionnelle seule ou combinée aux autres méthodes de programmation.
\end{encadrecolore}

\section{Fonction comme valeur passée à une autre fonction}

\subsection{Premier exemple}
Nous avons déjà utilisé la fonction \mintinline{python}{sorted} de \textsc{Python} pour trier des listes suivant différents critères. Par exemple pour trier une liste de paires d'\mintinline{python}{int} dans l'ordre croissant suivant leur deuxième élément, on écrit ceci :
\begin{pyc}
    \inputminted{python}{scripts/sort1.py}
\end{pyc}


On a bel et bien passé la fonction \mintinline{python}{f} \textit{comme paramètre} à la fonction \mintinline{python}{sorted}. Ainsi \mintinline{python}{f} est considérée comme une variable. D'ailleurs elle a un type :
\begin{pyc}
    \begin{minted}{python}
>>> type(f)

<class function>
\end{minted}
\end{pyc}

\subsection{Deuxième exemple}
La programmation fonctionnelle nous autorise à créer et utiliser des fonctions prenant d'autres fonctions comme paramètres :
\begin{pyc}
    \inputminted{python}{scripts/func1.py}
\end{pyc}

La fonction \mintinline{python}{make_table}
\begin{itemize}
    \item 	prend en paramètre une fonction \mintinline{python}{f} et deux \mintinline{python}{int} \mintinline{python}{a} et \mintinline{python}{b} ;
    \item 	renvoie une liste qui est le tableau de valeurs de \mintinline{python}{f} pour les valeurs entières entre \mintinline{python}{a} et \mintinline{python}{b} compris.
\end{itemize}
%

\subsection{Troisième exemple}
On peut imaginer des exemples plus utiles : Imaginons qu'on dispose d'une liste d'éléments $x_i$ et d'une opération \textit{associative}\footnote{c'est-à-dire que pour tous éléments $a$, $b$ et $c$ on a $(a\star b)\star c = a \star(b\star c)$} sur ces éléments notée $\star$.\\

On voudrait calculer $x_1\star x_2\ldots\star x_n$.\\

La fonction \mintinline{python}{compute} du programme suivant permet de le faire :
\begin{pyc}
    \inputminted{python}{scripts/func2.py}
\end{pyc}
\section{les fonctions lambda}
Il est assez fastidieux d'avoir à définir des fonctions à l'aide de \mintinline{python}{def}, lorsqu'elle sont juste destinées à être passées en paramètre.
Dans ce cas une \textit{fonction lambda} est plus simple à utiliser.\\
Reprenons l'exemple de \mintinline{python}{sorted} :

\begin{pyc}
    \begin{minted}{python}
>>> print(sorted([(1, 2), (3, 0), (5, 1)], key=lambda t: t[1]))

[(3, 0), (5, 1), (1, 2)]        
    \end{minted}
\end{pyc}


\begin{definition}[ : fonction lambda]
    Une fonction lambda (fonction anonyme) est une fonction définie à la volée et soumise à la syntaxe suivante :\\

    \mintinline[fontsize=\small]{python}{lambda var_1,...,var_n : <expression renvoyée utilisant ces variables>}\\

    Il n'y a pas de nom de fonction, \mintinline{python}{return} est sous-entendu et l'expression doit s'écrire comme une seule ligne.
\end{definition}

\begin{exemple}[]
    \begin{itemize}
        \item 	\mintinline{python}{lambda x, y : x *  y} pour le produit de 2 nombres ;
        \item 	pour faire la somme des 10 premiers éléments d'une liste :\\
              \mintinline{python}{lambda s : sum(s[i] for i in range(len(s)) if i < 10)}
    \end{itemize}
\end{exemple}

\section{Fonction comme valeur renvoyée par une fonction}

Avec une fonction lambda, on peut rapidement écrire une fonction \mintinline{python}{affine_function} qui
\begin{itemize}
    \item 	en entrée prend deux \mintinline{python}{float} \mintinline{python}{m} et \mintinline{python}{p} ;
    \item 	en sortie renvoie la fonction qui à tout \mintinline{python}{float} \mintinline{python}{x} renvoie le \mintinline{python}{float} \mintinline{python}{m*x+p};
\end{itemize}
\begin{pyc}
    \inputminted{python}{scripts/func3.py}
\end{pyc}

Rien ne nous oblige à utiliser une lambda fonction, mais c'est plus long sans :
\begin{pyc}
    \inputminted{python}{scripts/func4.py}
\end{pyc}

Rien n'interdit de créer une fonction prenant une fonction en paramètre et renvoyant une fonction en sortie.
\begin{pyc}
    \inputminted{python}{scripts/func5.py}
\end{pyc}
On peut même fabriquer des exemples vraiment intéressants.

\begin{propriete}[]
    Soit $f$ une fonction définie sur un intervalle I non réduit à un point et $x$ un réel appartenant à cet intervalle.\\

    On dit que $f$ est \textit{dérivable en} $x$ si et seulement si la quantité
    $$\dfrac{f(x+h)-f(x)}{h}$$  tend vers une valeur finie quand $x+h\in I$, $h\to 0$, $h\neq 0$.\\
    Lorsque c'est le cas on note $f'(x)$ ce nombre et on l'appelle \textit{nombre dérivé} de $f$ en $x$.\\

    Si $f$ est dérivable en tout réel $x\in I$ alors on peut définir la \textit{fonction} dérivée de $f$.
\end{propriete}
%

Plutôt qu'un passage à la limite, effectuons le calcul du taux d'accroissement $$\dfrac{f(x+h)-f(x)}{h}$$ pour une valeur de $h$ très petite, cela nous donnera dans beaucoup de cas une bonne approximation de $f'(x)$ et conduit à ceci :
\begin{pyc}
    \inputminted{python}{scripts/func6.py}
\end{pyc}


\section{La philosophie de la programmation fonctionnelle}

%{Origines de la programmation fonctionnelle}
Le plus ancien langage basé sur la programmation fonctionnelle est \textsc{Lisp}, inventé en 1958 par John McCarthy, dont on a déjà parlé précédemment.\\
Cependant ce paradigme de programmation découle directement des travaux d'Alonzo Church, mathématicien américain qui inventa le système du \textit{lambda-calcul} dans les années 30.

\begin{center}
    \includegraphics[width=3cm]{img/church}\\ \footnotesize\emph{Alonzo Church}
\end{center}

Le principe de la programmation fonctionnelle est de recourir le plus possible aux fonctions et de faire en sorte que celle-ci ne produisent pas d'\textit{effets de bord} (traduction un peu trop littérale du terme Anglais \textit{side effect} qui signifie en réalité \textit{effet secondaire}, dans le sens \emph{conséquence non prévue}).\\

L'idée est qu'une fonction évaluée avec un jeu de paramètres doit \textit{toujours donner le même résultat.}

Voici un exemple de fonction induisant ce qu'on appelle un « effet de bord ».

\begin{pyc}
    \inputminted{python}{scripts/func7.py}
\end{pyc}

Il n'est pas acceptable que l'évaluation de \mintinline{python}{f(10)} donne deux valeurs différentes !\\
Ainsi la programmation fonctionnelle interdit ce genre de fonctions.\\
L'effet de bord est dû au fait que le type \mintinline{python}{list} est \textit{mutable}, c'est-à-dire qu'on peut changer les éléments d'une liste après sa création.\\
Pour contourner le problème, la programmation fonctionnelle interdit les types mutables.\\
Dans un langage \textsc{Python} \textit{purement} fonctionnel, il n'y aurait pas de type \mintinline{python}{list}, simplement le type \mintinline{python}{tuple} (qui est non-mutable).\\

Suivant ce paradigme, l'exécution d'un programme est l'évaluation d'une ou plusieurs expressions, qui sont souvent des applications de fonctions à des valeurs passées en paramètre, tout en retenant bien que les fonctions elles-mêmes sont des valeurs.\\
La programmation fonctionnelle dite \textit{pure} bannit également l'idée de réaffectation : les variables créés et initialisées sont également non-mutables. C'est un peu comme si « les variables étaient des constantes ».\\
Hors de question d'écrire \mintinline{python}{i = i + 1} ou  bien même \mintinline{python}{for i in range(10)}. C'en est fini des boucles !\\
Dès lors on peut se demander s'il est vraiment possible de programmer suivant ce paradigme :
\begin{itemize}
    \item 	pas d'effets de bord ;
    \item 	des variables non-mutables ;
    \item 	pas de boucles.
\end{itemize}
La réponse est oui, et on a démontré que l'on peut programmer autant de choses qu'en suivant les autres paradigmes que sont la POO ou la programmation impérative. Voici un exemple d'algorithme programmé en impératif puis en fonctionnel
\begin{pyc}
    \begin{minted}{python}
        def somme(lst):
            s = 0
            for e in lst:
                s = s + e
            return s
        
        my_list = [1, 2, 3]
        print(somme(my_list))
        \end{minted}
\end{pyc}

\begin{pyc}
    \begin{minted}{python}
        def somme(lst):
            return 0 if not lst else lst[0] + somme(lst[1:])
        
        my_list = [1, 2, 3]
        print(somme(my_list))
        \end{minted}
\end{pyc}
On constate que le deuxième programme ne modifie aucune variable et ne contient pas de boucle, celle-ci a été remplacée par de la récursivité, c'est souvent le cas en programmation fonctionnelle.

\begin{encadrecolore}{Avantages de la programmation fonctionnelle}{UGLiGreen}
    \begin{itemize}
        \item 	les programmes sont plus concis ;
        \item 	on peut utiliser des \textit{fonctions d'ordre supérieur} c'est à dire des fonctions qui prennent d'autres fonctions en paramètre et/ou renvoient des fonctions en valeur de sortie ;
        \item 	le fait qu'il n'y ait pas d'effets de bords rend les programmes
              \begin{itemize}
                  \item 	plus sûrs ;
                  \item 	plus simple à déboguer ;
                  \item 	plus simples à prouver.
              \end{itemize}
    \end{itemize}

\end{encadrecolore}


\end{document}