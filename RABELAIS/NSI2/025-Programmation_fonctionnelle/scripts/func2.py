def compute(operator, lst: list) -> float:
    result = lst[0]
    for i in range(1, len(lst)):
        result = operator(result, lst[i])
    return result


def product(x: float, y: float) -> float:
    return x * y


print(compute(product, [1, 2, 3, 4, 5]))
# resultat : 120
