def double_function(f):
    return lambda x: 2 * f(x)


def g(x: float | int) -> float | int:
    return x * x


df = double_function(g)
print(df(10))  # resultat 200
