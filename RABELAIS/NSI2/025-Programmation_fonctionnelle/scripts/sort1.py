def f(t: tuple) -> int:
    return t[1]

lst1 = [(1, 2), (2, 0), (10, 1)]

# on prend chaque élément de lst1, on lui applique f
# suivant la valeur obtenue, on le place dans lst2
lst2 = sorted(lst1, key=f) 
print(lst2)

# resultat : [1, 3, 7, 13, 21, 31]