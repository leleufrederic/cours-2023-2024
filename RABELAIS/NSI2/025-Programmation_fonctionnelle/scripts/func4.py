def affine_function(m: float | int, p: float | int):
    def f(x: float | int) -> float | int:
        return m * x + p

    return f
