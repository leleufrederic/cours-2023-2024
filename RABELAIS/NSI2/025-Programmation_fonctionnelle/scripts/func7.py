lst = [1, 2]


def f(x: int) -> int:
    lst[0] += 1
    return x + lst[0]


print(f(10))  # result : 12
print(f(10))  # result : 13
