def make_table(f, a: int, b: int) -> list:
    """
    crée un tableau de valeurs de f en
    calculant les images des entiers de 
    a à b inclus
    """
    return [f(x) for x in range(a, b + 1)]


def g(x: float) -> float:
    return x ** 2 + x + 1


print(make_table(g, 0, 5))

# resultat : [1, 3, 7, 13, 21, 31]