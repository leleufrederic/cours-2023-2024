| Activité | Code Capytale |
|:---|:---:|
|001 Valeurs, types, affectations (C) |f8d9-1762244|
|002 Entrées, sorties et transtypage (C) |84b6-1768387|
|003 Structures conditionnelles (C) |5c0c-1768389|
|004 Structures itératives (C) |cd0b-1768417|
|005 Boucles for et graphiques (C) |a5df-1898072|
|005bis - RDI : algos du cours (C) |65e7-2189661|
|006 Listes - découverte (C) |bf78-1897945|
|007 Listes - activités (C) |0afd-1897967|
|007bis Boules rebondissantes (*) |b271-1964278|
|008 Fonctions (C) |9221-1959044|
|009 Juniper Green (C) |9a4a-1964082|
|010 Le jeu de la vie de Conway (C) |7df4-2382183|
|011 Listes en compréhension (C) |029f-2321021|
|012 Dictionnaires (C) |a5b4-2593234|
|014 Promenade au pays de l'unicode (C) |1664-2650037|
|015 Traitement des données en tables : partie 1 (C) |d44a-2661006|
|016 Traitement des données en table : partie 2, pandas (C) |8ad6-2661023|
|017 Répertoire de contacts (C) |256a-2660968|
|018 Mélange Américain (C) |7dcf-2800899|
|019 Guess Who ? (C) |9e61-2738735|
|019 Manipulation d'images et stéganographie (C) |ec69-2911599|
|020 Complexité (C) |57a0-3109992|
|021 Tris partie 1 : le tri par selection (C) |71ca-394503|
|022 Tris partie 2 : le tri par insertion (C) |dd1d-394528|
|023 Tris partie 3 : le tri à bulles (C) |2246-394554|
|024 Tris partie 4 : observations |692f-1422100|
|025 Tris partie 5 : invariants de boucle (C) |6d3f-3120096|
|026 Tris bonus : tri binaire (C) |0ebf-394563|
|027 Recherche dichotomique (C) |cea6-1520555|
|028 Algorithmes gloutons 01: Rendu de monnaie (C) |3e36-3120126|
|029 Algorithmes gloutons 02 : Problème des conférenciers (C) |d164-3120201|
|030 Copie de Algorithmes gloutons 03 : problème du sac à dos (C) |3c7e-3120252|
