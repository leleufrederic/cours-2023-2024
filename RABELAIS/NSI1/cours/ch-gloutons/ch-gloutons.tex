\chapter{Algorithmes gloutons}

\section{Un problème bien difficile}
On considère des villes A, B, ..., H. On veut toutes les visiter en partant d'une ville de départ et en y revenant.\\

On peut s'y prendre ainsi :
\begin{center}
    \includegraphics[width=7cm]{ch-gloutons/img/villes2.png}
\end{center}
Mais on minimisera sûrement la distance totale en s'y prenant ainsi :
\begin{center}
    \includegraphics[width=7cm]{ch-gloutons/img/villes1.png}
\end{center}
Cet exemple est assez simple : étant donné la configuration des villes, on est à peu près convaincus que la deuxième solution est la meilleure envisageable... Mais
\begin{itemize}
    \item	est-ce vraiment le cas ?
    \item	comment faire en général pour trouver ce meilleur itinéraire ?
\end{itemize}

Considérons maintenant le tableau suivant :
\begin{center}
    \tabstyle[UGLiOrange]
    \begin{tabular}{c|c|c|c|c|c|c|c}
        \cellcolor{white}    & \ccell Brest & \ccell Lorient & \ccell Nantes & \ccell Quimper & \ccell Rennes & \ccell Saint Brieuc & \ccell Vannes \\
        \ccell  Brest        &              & 134            & 297           & 71             & 242           & 146                 & 186           \\
        \ccell  Lorient      & 134          &                & 169           & 69             & 150           & 121                 & 58            \\
        \ccell  Nantes       & 297          & 169            &               & 232            & 109           & 204                 & 112           \\
        \ccell  Quimper      & 71           & 69             & 232           &                & 214           & 144                 & 121           \\
        \ccell  Rennes       & 242          & 150            & 109           & 214            &               & 99                  & 113           \\
        \ccell  Saint Brieuc & 146          & 121            & 204           & 144            & 99            &                     & 117           \\
        \ccell  Vannes       & 186          & 58             & 112           & 121            & 113           & 117                 &               \\
    \end{tabular}
\end{center}

Comment faire pour trouver le trajet le plus court qui passe par toutes ces villes en « bouclant » ?\\

Une première manière serait de calculer les longueurs de tous les trajets possibles (on parle de méthode \textit{brute force} en Anglais):
\begin{itemize}
    \item	on peut commencer par prendre une des 7 villes au hasard puisque chaque itinéraire passe par toutes les villes ;
    \item	ensuite il faut choisir la ville suivante et il y a 6 choix possibles ;
    \item   on continue : 5 choix possibles pour la ville suivante, \textit{et c\ae tera} jusqu'à la dernière ;
    \item   on a donc \textit{a priori} $6\times 5\times\ldots\times 2\times 1$ trajets à examiner, mais en fait en comptant ainsi, on a compté 2 fois chaque chemin : une fois dans un sens, une fois dans l'autre ;
    \item ainsi le nombre de possibilités à examiner est $\dfrac{6!}{2}$.
\end{itemize}
Le même raisonnement vaut pour $n$ villes et on doit explorer $\dfrac{(n-1)!}{2}$ trajets. Voici un tableau qui nous donne un aperçu
\begin{center}
    \tabstyle[UGLiOrange]
    \begin{tabular}{c|c}
        \ccell Nombre de villes & \ccell Nombre de trajets possibles \\
        5                       & 12                                 \\
        6                       & 60                                 \\
        7                       & 360                                \\
        8                       & \np{2520}                          \\
        10                      & \np{181440}                        \\
        15                      & plus de 43 milliards               \\
        20                      & plus de $6\cdot 10^{16}$           \\
        100                     & plus de $4\cdot 10^{155}$          \\
    \end{tabular}
\end{center}
Ce n'est donc pas raisonnable et on va devoir recourir à une autre stratégie. L'objectif n'est alors plus d'obtenir LE trajet le plus court, mais au moins un trajet qui est raisonnablement court.\\
On ne cherche pas une solution exacte, hors de portée, mais plutôt une solution approchée.

\section*{Une méthode alternative}

L'idée est de trouver un algorithme qui va faire, de proche en proche, le choix qui nous semble le meilleur... et c'est essentiellement ça, un algorithme glouton. On n'obtiendra peut-être pas le trajet le plus court, mais l'algorithme sera plus rapide.\\

Voici une idée simple et qui fonctionne bien : on part d'une ville choisie à l'avance et, à chaque fois, on choisit de visiter la ville la plus proche.\\

Partons de Saint Brieuc. La ville la plus proche est Rennes, puis la plus proche de Rennes (une fois Saint Brieuc écartée) est Nantes, puis de Nantes on va à Vannes, puis à Lorient, Quimper, Brest et on revient à Saint Brieuc, le tout pour un total de 664 kilomètres.\\

Par chance, il se trouve que c'est le trajet le plus court (avec évidemment celui qui fait la boucle dans l'autre sens) mais ce n'est pas, en général garanti.\\

Par contre, on peut prouver que le trajet obtenu grâce à cet algorithme est en général assez court même si ce n'est pas toujours le plus court, et ce pour un coût très raisonnable. En effet, pour $n$ villes :
\begin{itemize}
    \item	on en choisit une ;
    \item	ensuite on trouve la plus proche parmi les $n-1$ restantes ;
    \item   puis celle parmi les $n-2$ restantes ;
    \item \textit{et c\ae tera} jusqu'à la fin.
\end{itemize}
Lorsqu'on ajoute les comparaisons nécessaires pour trouver tous ces minima, on arrive à une quantité qui est de l'ordre de $n^2$, ce qui met les choses en perspectives :

\begin{center}
    \tabstyle[UGLiOrange]
    \begin{tabular}{c|c}
        \ccell Nombre de villes & \ccell Ordre de grandeur du nombre de comparaisons \\
        5                       & 25                                                 \\
        6                       & 36                                                 \\
        7                       & 49                                                 \\
        8                       & 64                                                 \\
        10                      & 100                                                \\
        15                      & 225                                                \\
        20                      & 400                                                \\
        100                     & \np{10000}                                         \\
    \end{tabular}
\end{center}


\section{Notion d'algorithme glouton}
\begin{center}
    \includegraphics[width=10cm]{ch-gloutons/img/glouton_van_dijkhuisen.png}\\
    Dessin d'Oscar V, ancien élève.
\end{center}
\begin{definition}[ : algorithme glouton]
    Un algorithme est dit \textit{glouton} lorsque
    \begin{itemize}
        \item 	il procède étape par étape, avec une boucle ;
        \item 	à chaque itération il essaye d'\textit{optimiser} une grandeur (maximiser ou minimiser) en faisant un \textit{choix} ;
        \item 	les choix faits sont \textit{définitifs} : ils ne sont jamais remis en questions lors des itérations suivantes.
    \end{itemize}
\end{definition}

\begin{exemple}[ : rendu de monnaie]
    Lorsqu'on rend la monnaie en euros et qu'on veut rendre le moins de pièces (ou billets) possibles, on
    \begin{itemize}
        \item 	procède pièce par pièce ;
        \item  	choisit la pièce dont la valeur est la plus grande possible tout en restant inférieure ou égale au montant qu'il reste à rendre ;
        \item 	on continue ainsi jusqu'à ce qu'il ne reste plus rien à rendre, sans jamais reprendre une pièce rendue auparavant.
    \end{itemize}
    Cette méthode est gloutonne et elle permet toujours de rendre la monnaie avec le moins de pièces possible (en tout cas lorsque le système monétaire est l'euro).
\end{exemple}
\section{Une méthode pas toujours optimale\ldots}


Considérons un robot placé en A, qui veut monter le plus haut possible.
S'il applique la méthode gloutonne suivante :\medskip\par
\picright{0.33}{ch-gloutons/img/glouton}{
    \begin{itemize}
        \item à chaque seconde, tant que possible ;
        \item regarder à droite ou à gauche sur une petite distance ;
        \item aller dans la direction ou la pente est la plus forte.
    \end{itemize}
}\medskip\par
Alors il se retrouvera en m, et pas en M.

\begin{aretenir}
    \begin{itemize}
        \item  un algorithme glouton ne fournit pas toujours une solution optimale ;
        \item pour s'assurer qu'il fournit une démonstration optimale, il faut le \textit{démontrer}.
    \end{itemize}
\end{aretenir}
Parmi les méthodes gloutonnes non optimales, on trouve

\begin{itemize}
    \item celle exposée en introduction : trouver un trajet de longueur minimal pour « faire une boucle » ;
    \item le problème du robot exposé précédemment ;
    \item les méthodes gloutonnes pour résoudre le problème du « sac à dos » (voir les TP).
\end{itemize}


\section{Et parfois, si !}
Il existe néanmoins des problèmes pour lesquels un algorithme glouton fournit une solution optimale :
\begin{itemize}
    \item le rendu de pièces en euros ;
    \item l'écriture d'un entier naturel en binaire par la méthode des soustractions (qui correspond à un rendu de pièces qui ont des valeurs de $2^n$) ;
    \item l'algorithme dit « des conférenciers » ;
    \item l'algorithme de codage de Huffman (utilisé dans les algorithmes de compression actuels).
\end{itemize}
