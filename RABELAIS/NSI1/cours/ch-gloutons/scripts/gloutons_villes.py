def permutation(lst):
    # If lst is empty then there are no permutations
    if len(lst) == 0:
        return []

    # If there is only one element in lst then, only
    # one permutation is possible
    if len(lst) == 1:
        return [lst]

    # Find the permutations for lst if there are
    # more than 1 characters

    l = []  # empty list that will store current permutation

    # Iterate the input(lst) and calculate the permutation
    for i in range(len(lst)):
        m = lst[i]

        # Extract lst[i] or m from the list.  remLst is
        # remaining list
        remLst = lst[:i] + lst[i + 1:]

        # Generating all permutations where m is first
        # element
        for p in permutation(remLst):
            l.append([m] + p)
    return l


def longueur(trajet: list):
    total = distance[trajet[-1]][trajet[0]]
    for i in range(len(trajet) - 1):
        v1, v2 = trajet[i], trajet[i + 1]
        total += distance[v1][v2]
    return total, trajet + [trajet[0]]


def glouton(depart):
    total = 0
    resultat = [depart]
    villes_restantes = (list(distance.keys()))
    villes_restantes.remove(depart)
    courant = depart
    while villes_restantes:
        suivant,d = min((x for x in distance[courant].items() if x[0] in villes_restantes), key=lambda x: x[1])
        total += d
        courant = suivant

        villes_restantes.remove(courant)
        resultat.append(courant)
    resultat.append(depart)
    total+= distance[courant][depart]
    return total,resultat

distance = {'B': {'L': 134, 'N': 297, 'Q': 71, 'R': 242, 'S': 146, 'V': 186},
            'L': {'N': 169, 'Q': 69, 'R': 150, 'S': 121, 'V': 58},
            'N': {'Q': 232, 'R': 109, 'S': 204, 'V': 112},
            'Q': {'R': 214, 'S': 144, 'V': 121},
            'R': {'S': 99, 'V': 113},
            'S': {'V': 117},
            'V': {}}

for ville1 in distance:
    for ville2 in distance[ville1]:
        distance[ville2][ville1] = distance[ville1][ville2]

villes = list(distance.keys())

combinaisons = permutation(villes)

trajets = [longueur(t) for t in combinaisons]
print(min(trajets), max(trajets))
print(glouton('S'))
