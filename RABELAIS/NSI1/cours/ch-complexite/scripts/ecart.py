from random import randint

def plus_rapproches(lst: list) -> tuple:
    mini = float('inf')
    for i in range(len(lst)):
        for j in range(i+1,len(lst)):
            if abs(lst[i]-lst[j]) < mini:
                mini = abs(lst[i]-lst[j])
                result = lst[i],lst[j]
    return result


lst = list(set(randint(-10000,10000) for _ in range(100)))

print(plus_rapproches(lst))