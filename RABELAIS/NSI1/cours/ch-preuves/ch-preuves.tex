\chapter{Preuve d'un algorithme}

Un algorithme est censé
\begin{itemize}
    \item prendre des données en entrée ;
    \item calculer / produire un résultat en fonction de ces données ;
\end{itemize}

Pour être sûr qu'un algorithme fait bien ce qui est annoncé il faut \textit{prouver} que quelles que soient les données d'entrée, l'algorithme produise le résultat attendu. C'est comme en mathématiques.
Cela sert à
\begin{itemize}
    \item assurer la sécurité lorsque le programme est critique (pilotages automatiques, freinage assisté, régulateurs de vitesses) ;
    \item la même chose qu'en mathématiques : un algorithme prouvé est un résultat établi dont on peut se servir pour « aller plus loin ».
\end{itemize}


Est-ce qu'on a prouvé que le programme qui interprète le langage Python est correct ? Non !\\
Certains compilateurs (pour le langage C entre autres) ont cependant été prouvés.

\section{Une méthode de preuve}

Quand un algorithme comporte des boucles (ce qui arrive la plupart du temps) un moyen de le prouver est
\begin{itemize}
    \item d'abord, prouver sa \textit{terminaison}, c'est-à-dire essentiellement que les boucles \texttt{tant que} se terminent ;
    \item ensuite, prouver sa \textit{correction} :
          \begin{itemize}
              \item montrer qu'à chaque itération de boucle, une propriété est vérifiée ;
              \item vérifier que la propriété qui est vérifiée à la dernière itération est la bonne.
          \end{itemize}
\end{itemize}
Cette propriété qui sert à prouver la correction s'appelle un \emph{invariant de boucle}.
\begin{definition}
	Un \textit{invariant de boucle} est une propriété $\mathcal{P}$ dépendant éventuellement des variables du programme. 
	\begin{itemize}
		\item 	$\mathcal{P}$ doit être vraie avant l'entrée dans la boucle ;
		\item 	$\mathcal{P}$ doit rester vraie à chaque itération de boucle ;
		\item 	à la fin de la boucle, $\mathcal{P}$ doit nous permettre de conclure que la fonction « fait bien ce qu'elle doit faire ».
	\end{itemize}
\end{definition}



\section{Preuve du tri par insertion}
Prouvons que la fonction suivante trie toute liste d'entiers dans l'ordre croissant
\begin{minted}[fontsize=\small]{python}
def tri(lst: list[int]) -> None:
    n = len(lst)
    for i in range(1, n):
        j = i
        while 0 < j and lst[j - 1] > lst[j]:
            lst[j - 1], lst[j] = lst[j], lst[j - 1]
            j -= 1
\end{minted}

\subsection{Terminaison}
Chaque boucle \mintinline{python}{while} se termine :\\

En effet :
\begin{itemize}
    \item soit parmi tous les éléments de \mintinline{python}{lst[:i]} il y en a un plus petit que \mintinline{python}{lst[i]} et à ce moment la boucle se termine ;
    \item sinon la valeur de \mintinline{python}{j} diminue de 1 à chaque itération de boucle donc va finir par devenir nulle et la boucle va se terminer.\\
\end{itemize}

Ainsi on a prouvé la terminaison de la fonction \mintinline{python}{tri}.


\subsection{Correction}
Soit $n$ la longueur de la liste.\\
Si $n$ vaut 0 ou 1 il n'y a rien à prouver.\\
Sinon pour tout $i$ entre 0 et $n-1$ on appelle $P_i$ la propriété suivante :
\begin{center}
    « Tous les éléments de \mintinline{python}{lst[0]} à \mintinline{python}{lst[i]} sont triés. » \\
\end{center}
ce qui se réécrit
\begin{center}
    «\mintinline{python}{lst[i+1:]} est triée.»
\end{center}
$P_i$ est un invariant de boucle.\\
Montrons qu'à la i\eme itération de la boucle \mintinline{python}{for}, la propriété $P_i$ est vérifiée.


\begin{itemize}
    \item $P_0$ est vérifiée à l'itération 0, c'est-à-dire avant même de rentrer dans la boucle : \ en effet la liste \mintinline{python}{[lst[0]]} est triée ;
    \item si on suppose que $P_i$ à la i\eme itération, alors :
          \begin{itemize}
              \item au début de l'itération i+1, on sait que \mintinline{python}{lst[:i+1]} est triée ;
              \item comme expliqué précédemment la boucle \mintinline{python}{while} a pour effet d'insérer \mintinline{python}{lst[i+1]} dans \mintinline{python}{lst[:i+1]} soit au début de la liste si cet élément est le plus petit, soit après le premier élément plus petit que lui.\\
                    Puisque la liste \mintinline{python}{lst[:i+1]} est triée, cette insertion produit donc une liste \mintinline{python}{lst[i+2:]} triée.
          \end{itemize}
\end{itemize}

Ainsi à la fin de l'itération i+1 on a la propriété $P_{i+1}$.\\

En conclusion à la fin de la boucle \mintinline{python}{for}, c'est-à-dire à l'itération n-1, on a $P_{n-1}$ : \\

\mintinline{python}{lst[:n]} c'est à dire \mintinline{python}{lst}, est triée.\\
oo
Ceci achève la preuve de la correction de la fonction \mintinline{python}{tri}.

\begin{exercice}[]
    On considère la fonction suivante

    \begin{minted}{python}
    def tri_bulles(lst: list[int]) -> int:
        echange = True
        while echange:
            echange = False
            for i in range(len(lst) - 1):
                if lst[i] > lst[i + 1]:
                    echange = True
                    lst[i], lst[i + 1] = lst[i + 1], lst[i]
    \end{minted}

    \begin{enumerate}
        \item	Montrer que la boucle \mintinline{python}{while} s'arrête dès que \mintinline{python}{lst} est triée dans l'ordre croissant et pas avant.
        \item	Trouver un invariant de boucle (on ne demande pas de prouver que c'en est un).
    \end{enumerate}  
\end{exercice}

