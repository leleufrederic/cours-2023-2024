def tri_bulles(lst: list[int]) -> int:
    echange = True
    while echange:
        echange = False
        for i in range(len(lst) - 1):
            if lst[i] > lst[i + 1]:
                echange = True
                lst[i], lst[i + 1] = lst[i + 1], lst[i]
    
