data = []
go_on = True
with open("RABELAIS/NSI1/FOUILLIS/Activités Capytale/capytale.txt", "rt", encoding="utf8") as rf:
    while go_on:
        a = rf.readline().strip('\n')
        rf.readline()
        b = rf.readline().strip('\n')
        data.append((a, b))
        rf.readline()
        rf.readline()
        if not rf.readline():
            go_on = False

print('| Activité | Lien |')
print('|:---|:---:|')
for a in data:
    print("| "+a[0]+""" | <a href="https://capytale2.ac-paris.fr/web/c/"""+a[1]+"""">ici</a> |""")