lst = [['*' if i > j else '.' for j in range(10)] for i in range(10)]

for l in lst:
    print(" ".join(l))
