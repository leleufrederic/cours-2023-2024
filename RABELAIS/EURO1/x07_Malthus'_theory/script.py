for i in range(50):
    P = round(10 * 1.03 ** i, 2)
    R = round(12 + .48 * i, 2)
    print(f"Year {1800 + i}, Population {P}, Feed Resources {R}.", end=" ")
    if P <= R:
        print("Resources are sufficient.")
    else:
        print("INSUFFICIENT RESOURCES.")
