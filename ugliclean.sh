#!/bin/bash

# "nav" "aux" "log" "out" "snm" "vrb" "toc" "synctex.gz" "pygtex" "pygstyle"
echo "Starting Deletion..."
find . -type d -name "*_minted*"  -print -exec rm -rf {} +
find . -type f -name ".DS_Store" -print -exec rm -rf {} +
find . -type f -name "*.nav" -print -exec rm -rf {} +
find . -type f -name "*.aux" -print -exec rm -rf {} +
find . -type f -name "*.log" -print -exec rm -rf {} +
find . -type f -name "*.out" -print -exec rm -rf {} +
find . -type f -name "*.snm" -print -exec rm -rf {} +
find . -type f -name "*.vrb" -print -exec rm -rf {} +
find . -type f -name "*.toc" -print -exec rm -rf {} +
find . -type f -name "*.synctex.gz" -print -exec rm -rf {} +
find . -type f -name "*.pygtex" -print -exec rm -rf {} +
find . -type f -name "*.pygstyle" -print -exec rm -rf {} +
find . -type f -name "*:Zone.Identifier" -print -exec rm -rf {} +

echo "Deletion Complete !"
